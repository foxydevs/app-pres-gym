import { Component } from '@angular/core';
import { NavController, IonicPage, ViewController} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { path } from '../../../../app/config.module';
import { LoadingController } from '../../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { AddressService } from '../../../../app/service/address.service';

@IonicPage()
@Component({
  selector: 'modal-address-client',
  templateUrl: 'modal-address-client.html',
})
export class ModalAddressUserPage {
  //PROPIEDADES
  public parameter:any;
  public title:any;
  public disabledBtn:any;
  public basePath:string = path.path;
  public baseUserId = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  public data = {
    direccion: '',
    ciudad: '',
    numero: '',
    calle: '',
    observacion: '',
    app: path.id,
    client: localStorage.getItem('currentId'),
    id: '',
    tipo: 0,
    state: 0
  }

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toast: ToastController,
    public loading: LoadingController,
    public mainService: AddressService,
    public view: ViewController
  ) {
    this.parameter = this.navParams.get('parameter')
    if(this.parameter) {
      this.title = "Edición Dirección";
      this.getSingle(this.parameter);     
    } else {
      this.title = "Agregar Dirección";
    }
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    if(this.data.direccion) {
      if(this.parameter) {
        console.log(this.data)
        this.update(this.data)
      } else {
        console.log(this.data)
        this.create(this.data)
      }
    } else {
      this.message('La dirección es requerida.');
    }
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .then(response => {
      this.confirmation('Dirección Agregada', 'La dirección fue agregada exitosamente.');
      this.data.id = response.id;
      this.parameter = response.id;
      this.title = "Edición Blog";
      this.view.dismiss(response);
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .then(response => {
      this.confirmation('Dirección Actualizada', 'La dirección fue actualizada exitosamente.');
      this.view.dismiss(response);
      console.log(response);
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar la dirección?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando..."
            });
            load.present();
            this.mainService.delete(id)
            .then(response => {
              load.dismiss();
              this.view.dismiss(response);
              console.clear();
            }).catch(error => {
              console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss('Close');
  }
}
