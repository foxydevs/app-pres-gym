import { Component } from '@angular/core';
import { NavController, IonicPage, ModalController } from 'ionic-angular';
import { UsersService } from '../../../app/service/users.service';

@IonicPage()
@Component({
  selector: 'profile-client',
  templateUrl: 'profile-client.html'
})
export class ProfileClientPage {
  //Propiedades
  public profilePicture:any;
  public user:any;
  public email:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  UUID = localStorage.getItem('currentUUID');

  constructor(
    public navCtrl: NavController,
    public usersService: UsersService,
    public modalController: ModalController
  ) {
    this.getSingle();
    if(localStorage.getItem('currentState') == '21') {
      this.changePassword();
    }
    if(this.UUID!='0') {
      console.log('MOSTRAR')
    } else {
      console.log(this.UUID)      
    }
  }

  //MI PERFIL
  public updateProfile() {
    let parameter = '1'
    let chooseModal = this.modalController.create('ProfileConfigurationClientPage', { parameter });
    chooseModal.onDidDismiss(data => {
      console.log(data)
      this.getSingle();
    });
    chooseModal.present();
  }

  //CAMBIAR CONTRASEÑA
  public changePassword() {
    let parameter = '2'
    let chooseModal = this.modalController.create('ProfileConfigurationClientPage', { parameter });
    chooseModal.onDidDismiss(data => {
      this.getSingle();
    });
    chooseModal.present();
  }

  //DIRECCION
  public addressClient() {
    let parameter = '5'
    let chooseModal = this.modalController.create('ProfileConfigurationClientPage', { parameter });
    chooseModal.onDidDismiss(data => {
      this.getSingle();
    });
    chooseModal.present();
  }

  //MEMBRESÍA
  public membership() {
    let parameter = '4'
    let chooseModal = this.modalController.create('ProfileConfigurationClientPage', { parameter });
    chooseModal.onDidDismiss(data => {
      this.getSingle();
    });
    chooseModal.present();
  }

  //GET DATA
  getSingle() {
    this.profilePicture = localStorage.getItem("currentPicture");
    this.user = localStorage.getItem("currentFirstName") + ' ' + localStorage.getItem("currentLastName");
    this.email = localStorage.getItem('currentEmail');
  }

  ionViewWillEnter() {
    this.getSingle();
  }

}