import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';

@IonicPage()
@Component({
  selector: 'my-cart-client',
  templateUrl: 'my-cart-client.html'
})
export class MyCartClientPage {
  cartProducts:any[] = [];
  btnDisabled:boolean;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  authentication:any = localStorage.getItem('currentAuthentication');

  constructor(public navCtrl: NavController,
  public loading: LoadingController,
  public alertCtrl: AlertController,
  public toast: ToastController,
  public modal: ModalController) {
  }

  public deleteCar(e:any) {
    let confirm = this.alertCtrl.create({
      title: '¿Desea eliminar el producto del carrito?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.cartProducts.splice(this.cartProducts.indexOf(e),1)
            localStorage.removeItem('cart');
            localStorage.setItem('cart', JSON.stringify(this.cartProducts))
            if(this.cartProducts.length == 0) {
              this.btnDisabled = true;
            } else {
              this.btnDisabled = false;
            }
          }
        }
      ]
    });
    confirm.present();
  }

  public addMore(e:any) {
    console.log('ANTES', e)
    let cant = parseInt(e.cantidad) + 1;
    let sub = cant * e.price;
    sub.toFixed(2);
    let data = {
      cantidad: cant,
      name: e.name,
      subtotal: sub,
      price: e.price,
      precioVenta: e.precioVenta,
      precioClienteEs: e.precioClienteEs,
      precioDistribuidor: e.precioDistribuidor, 
      producto: e.producto,
      picture: e.picture,
      opcion1: e.opcion1,
      opcion2: e.opcion2,
      opcion3: e.opcion3,
      opcion4: e.opcion4,
      opcion5: e.opcion5,
      opcion6: e.opcion6,
      presentacion: e.presentacion,
      comment: e.comment,
    }
    console.log('DESPUES', data);
    for (var x in this.cartProducts) {
      if (this.cartProducts[x] == e) {
        this.cartProducts[x] = data;
        console.log("EDICION", this.cartProducts[x])
        localStorage.setItem('cart', JSON.stringify(this.cartProducts));
        break; //Stop this loop, we found it!
      }
    }
  }

  public removeProduct(e:any) {
    console.log('ANTES', e)
    let cant = parseInt(e.cantidad) - 1;
    let sub = cant * e.price;
    sub.toFixed(2);
    let data = {
      cantidad: cant,
      name: e.name,
      subtotal: sub,
      price: e.price,
      precioVenta: e.precioVenta,
      precioClienteEs: e.precioClienteEs,
      precioDistribuidor: e.precioDistribuidor, 
      producto: e.producto,
      picture: e.picture,
      opcion1: e.opcion1,
      opcion2: e.opcion2,
      opcion3: e.opcion3,
      opcion4: e.opcion4,
      opcion5: e.opcion5,
      opcion6: e.opcion6,
      comment: e.comment,
      presentacion: e.presentacion,
    }
    console.log('DESPUES', data);
    for (var x in this.cartProducts) {
      if (this.cartProducts[x] == e) {
        if(this.cartProducts[x].cantidad > 1) {
          this.cartProducts[x] = data;
          console.log("EDICION", this.cartProducts[x])
          localStorage.setItem('cart', JSON.stringify(this.cartProducts));
        }        
        break; //Stop this loop, we found it!
      }
    }
  }

  //CONFIRMAR CARRITO
  public confirmateCart() {
    if(localStorage.getItem('currentAuthentication') == 'Authentication') {
      console.log('Usted esta logeado!...')
      this.loading.create({
        content: "Cargando...",
        duration: 500
      }).present();
      this.navCtrl.push('MyCartOrderClient');
    } else {
      let confirm = this.alertCtrl.create({
        title: 'Necesita Iniciar Sesión',
        buttons: [
          {
            text: 'OK',
            handler: () => {
              this.navCtrl.setRoot('LoginPage')
            }
          }
        ]
      });
      confirm.present();
    }    
  }

  public seeMore(parameter:any) {
    if(this.authentication == 'NoAuthentication') {
      this.message('Usted debe iniciar sesión.')
    } else {
      this.navCtrl.push('SeeProductsClientPage', { parameter });
    }
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 1000
    }).present();
  }

  ionViewWillEnter() {
    this.cartProducts = JSON.parse(localStorage.getItem('cart'));
    console.log(this.cartProducts)
    if(this.cartProducts.length == 0) {
      this.btnDisabled = true;
    } else {
      this.btnDisabled = false;
    }
  }

}