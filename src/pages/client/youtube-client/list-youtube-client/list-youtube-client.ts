/**
 * @author    ThemesBuckets <themebucketbd@gmail.com>
 * @copyright Copyright (c) 2018
 * @license   Fulcrumy
 * 
 * This File Represent Playlist Items Component
 * File path - '../../src/pages/playlist-items/playlist-items'
 */

import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import * as _ from 'lodash';
import { YoutubeProvider } from '../../../../providers/youtube/youtube';


@IonicPage()
@Component({
  selector: 'list-youtube-client',
  templateUrl: 'list-youtube-client.html',
})
export class ListYoutubeClientPage {
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  // Playlist Id
  playlistId: any;

  // List of Videos
  videos: any = [];

  // Total Result of Playlist Items
  totalResults: any;

  // Next Page Token
  nextPageToken: any = '';

  // Track Next Page Video is Exist or Not
  finished: Boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public youtubeProvider: YoutubeProvider) {

    // Get Playlist Id
    this.playlistId = this.navParams.get('playlistId');
  }

  /** Do any initialization */
  ngOnInit() {
    this.getPlaylistItems('');
  }

  /**
   * --------------------------------------------------------------
   * Get All Playlist Items
   * --------------------------------------------------------------
   * @param token     Next page token Id
   */
  async getPlaylistItems(token) {
    this.youtubeProvider.getPlaylistItems(this.playlistId, token).subscribe(data => {

      this.videos = _.uniqBy(this.videos.concat(data.items), 'id');
      this.totalResults = data.pageInfo.totalResults;

      if (data.nextPageToken) {
        this.nextPageToken = data.nextPageToken;
      } else {
        this.finished = true;
      }
    })
  }

  /**
   * --------------------------------------------------------------
   * Video Details Page
   * --------------------------------------------------------------
   * @param video 
   * This method open a modal that represent specific video details page.
   */
  openVideo(video) {
    //let modal = this.modalCtrl.create(SeeYoutubeUserPage, { videoId: video.contentDetails.videoId });
    //modal.present();
    this.navCtrl.push('SeeYoutubeClientPage', { videoId: video.contentDetails.videoId })
  }

  /**
   * --------------------------------------------------------------
   * Infinite Scroll
   * --------------------------------------------------------------
   * @method doInfinite
   * 
   * The Infinite Scroll allows to perform an action when the user
   * scrolls a specified distance from the bottom or top of the page,
   * and load next page videos
   */
  doInfinite(event) {
    setTimeout(() => {
      this.getPlaylistItems(this.nextPageToken).then(() => {
        event.complete();
      });
    }, 1000);
  }
}
