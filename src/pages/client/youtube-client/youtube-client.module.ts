import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YoutubeClientPage } from './youtube-client';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    YoutubeClientPage,
  ],
  imports: [
    IonicPageModule.forChild(YoutubeClientPage),
    PipeModule
  ],
  exports: [
    YoutubeClientPage
  ]
})
export class YoutubeClientPageModule {}