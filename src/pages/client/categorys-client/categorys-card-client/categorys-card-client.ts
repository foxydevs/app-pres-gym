import { Component, ViewChild } from '@angular/core';
import { NavController, LoadingController, AlertController, IonicPage, Slides } from 'ionic-angular';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { CategorysService } from '../../../../app/service/categorys.service';
import { UsersService } from '../../../../app/service/users.service';
import { path } from '../../../../app/config.module';

@IonicPage()
@Component({
  selector: 'categorys-card-client',
  templateUrl: 'categorys-card-client.html'
})
export class CategorysCardClientPage {
  @ViewChild(Slides) slides: Slides;
  //Propiedades
  public categorys:any[] = [];
  public idUser:any;
  public pictureCategories:any = localStorage.getItem('currentPictureCategories');
  public baseId = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(
    public navCtrl: NavController,
    public categorysService: CategorysService,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public usersService: UsersService,
    public modalController: ModalController,
  ) {
    this.idUser = localStorage.getItem("currentId");
    if(localStorage.getItem('currentState') == '21') {
      this.openModalCreate();
    }
    this.getAll();
  }

  public ngOnInit() {
    this.slides.effect = 'cube';
  }

  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    return currentIndex;
  }

  //Cargar los productos
  public getAll(){
    this.categorysService.getAllUser(this.baseId)
    .then(response => {
      this.categorys = [];
      this.categorys = response;
    }).catch(error => {
      console.clear
    })
  }

  //Ver Productos de la Categoria
  public seeProducts(parameter:any) {
    this.navCtrl.push('ProductsClientPage', { parameter });
  }

  ionViewWillEnter() {
    this.goToSlide(+this.slideChanged());
    this.slides.effect = 'cube';
    setTimeout(() => {
      this.goToSlide(this.slideChanged());
    }, 1000);
  }

  goToSlide(id:number) {
    this.slides.slideTo(id, 1000);
  }

  //ABIR MODAL AGREGAR
  public openModalCreate() {
    let parameter = {
      state: '2',
      id: null
    }
    let chooseModal = this.modalController.create('ProfileConfigurationUserPage', { parameter });
    chooseModal.present();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll();
      refresher.complete();
    }, 2000);
  }

}
