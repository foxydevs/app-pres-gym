import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategorysClientPage } from './categorys-client';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    CategorysClientPage,
  ],
  imports: [
    IonicPageModule.forChild(CategorysClientPage),
    PipeModule
  ],
  exports: [
    CategorysClientPage
  ]
})
export class CategorysClientPageModule {}