import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyProgressClientPage } from './my-progress-client';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    MyProgressClientPage,
  ],
  imports: [
    IonicPageModule.forChild(MyProgressClientPage),
    PipeModule
  ],
  exports: [
    MyProgressClientPage
  ]
})
export class MyProgressClientPageModule {}