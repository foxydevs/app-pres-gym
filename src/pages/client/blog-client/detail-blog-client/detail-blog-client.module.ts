import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailBlogClientPage } from './detail-blog-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    DetailBlogClientPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailBlogClientPage),
    PipeModule
  ],
  exports: [
    DetailBlogClientPage
  ]
})
export class DetailBlogClientPageModule {}