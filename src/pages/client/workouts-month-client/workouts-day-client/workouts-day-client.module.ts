import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WorkoutsDayClientPage } from './workouts-day-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    WorkoutsDayClientPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkoutsDayClientPage),
    PipeModule
  ],
  exports: [
    WorkoutsDayClientPage
  ]
})
export class WorkoutsDayClientPageModule {}