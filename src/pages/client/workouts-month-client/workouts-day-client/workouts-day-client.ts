import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, IonicPage } from 'ionic-angular';
import { LoadingController } from '../../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { PeriodosService } from '../../../../app/service/periodos.service';

@IonicPage()
@Component({
  selector: 'workouts-day-client',
  templateUrl: 'workouts-day-client.html'
})
export class WorkoutsDayClientPage {
  //PROPIEDADES
  table:any[] = [];
  parameter:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public mainService: PeriodosService,
    public navParams: NavParams,
    public modal: ModalController,
  ) {
  }

  //OBTENER DATOS
  getAll(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.table = []
      this.table = response.childs;
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  openForm(parameter:any) {
    this.navCtrl.push('RutinasClientPage', { parameter })
  }

  ionViewWillEnter() {
    this.parameter = this.navParams.get('parameter');
    this.getAll(this.parameter);
  }
}