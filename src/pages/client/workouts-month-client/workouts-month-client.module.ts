import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WorkoutsMonthClientPage } from './workouts-month-client';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    WorkoutsMonthClientPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkoutsMonthClientPage),
    PipeModule
  ],
  exports: [
    WorkoutsMonthClientPage
  ]
})
export class WorkoutsMonthClientPageModule {}