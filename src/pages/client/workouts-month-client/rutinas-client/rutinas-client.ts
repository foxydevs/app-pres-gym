import { Component } from '@angular/core';
import { NavController, IonicPage} from 'ionic-angular';
import { LoadingController } from '../../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { ModalController } from '../../../../../node_modules/ionic-angular/components/modal/modal-controller';
import { NavParams } from '../../../../../node_modules/ionic-angular/navigation/nav-params';
import { RutinasService } from '../../../../app/service/rutinas.service';
import { path } from '../../../../app/config.module';

@IonicPage()
@Component({
  selector: 'rutinas-client',
  templateUrl: 'rutinas-client.html',
})
export class RutinasClientPage {
  //PROPIEDADES
  public table:any[] = [];
  public idUser:any = path.id;
  public idClient:any = localStorage.getItem('currentId');
  public parameter:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public modal: ModalController,
    public mainService: RutinasService,
    public navParams: NavParams
  ) {
    this.parameter = this.navParams.get('parameter');
  }
  
  //CARGAR LOS RETOS
  public getAll(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllUser(id)
    .then(response => {
      this.table = []
      this.table = response;
      console.log(this.table)
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  ionViewWillEnter() {
    this.getAll(this.idUser);
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll(this.idUser);
      refresher.complete();
    }, 2000);
  }

  openForm(parameter?:any) {
    this.navCtrl.push('DetailWorkoutsClientPage', { parameter });
  }

}
