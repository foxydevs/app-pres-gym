import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RutinasClientPage } from './rutinas-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    RutinasClientPage,
  ],
  imports: [
    IonicPageModule.forChild(RutinasClientPage),
    PipeModule
  ],
  exports: [
    RutinasClientPage
  ]
})
export class RutinasClientPageModule {}