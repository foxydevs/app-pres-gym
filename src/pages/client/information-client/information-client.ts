import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { UsersService } from '../../../app/service/users.service';
import { path } from '../../../app/config.module';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { InformationService } from '../../../app/service/information.service';
import { ScheduleService } from '../../../app/service/schedule.service';
import { IonicPage } from '../../../../node_modules/ionic-angular/navigation/ionic-page';
import { CallNumber } from '@ionic-native/call-number';
import { AndroidPermissions } from '../../../../node_modules/@ionic-native/android-permissions';

//JQUERY
declare var google;

@IonicPage()
@Component({
  selector: 'information-client',
  templateUrl: 'information-client.html'
})
export class InformationClientPage {
  //PROPIEDADES
  public data:any = [];
  public schedules:any = [];
  public information:any = [];
  public profilePicture:any;
  public portadaPicture:any;
  public map: any;
  public idUser:any = path.id;
  public last_latitud:any;
  public last_longitud:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  pictureLogin:string = localStorage.getItem('currentPictureLogin');
  design = localStorage.getItem('currentDesign');
  
  constructor(
    public navCtrl: NavController,
    public userService: UsersService,
    public iab: InAppBrowser,
    public mainService: InformationService,
    public secondService: ScheduleService,
    public modal: ModalController,
    private callNumber: CallNumber,
    public androidPermissions: AndroidPermissions
  ) {
    this.userService.getSingle(this.idUser)
    .then(response => {
    this.profilePicture = response.picture;
    this.portadaPicture = response.pic3;
    this.last_latitud = response.last_latitud;
    this.last_longitud = response.last_longitud;
    this.data = response;
      this.loadMapUpdate(this.last_latitud, this.last_longitud);
    }).catch(error => {
        console.clear()
    })
  }

  //VER INFORMACION
  public openModal() {
    let chooseModal = this.modal.create("InformationModalClientPage", null, {
      cssClass:"my-modal"
    });
    chooseModal.present();
  }

  //VER INFORMACION
  public openModalUbication() {
    let chooseModal = this.modal.create("UbicationModalClientPage");
    chooseModal.present();
  }

  //VER INFORMACION
  public openModalHorary() {
    let chooseModal = this.modal.create("HoraryModalClientPage");
    chooseModal.present();
  }

  public call(phone:string) {
    this.permission();
    this.callNumber.callNumber(phone, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }

  //CARGAR USUARIOS
  public getAll(){
    this.mainService.getAllUser(this.idUser)
    .then(response => {
      this.information = [];
      this.information = response;
    }).catch(error => {
      console.clear;
    })
  }

  //CARGAR USUARIOS
  public getAllSecond(){
    this.secondService.getAllUser(this.idUser)
    .then(response => {
      this.schedules = [];
      this.schedules = response;
    }).catch(error => {
      console.clear;
    })
  }

  //CARGAR MAPA ACTUALIZAR
  public loadMapUpdate(lat:any, lon:any) {
  let latitude = lat;
  let longitude = lon;
  this.last_latitud = latitude.toString();
  this.last_longitud = longitude.toString();
    let mapEle: HTMLElement = document.getElementById('map');
    let myLatLng = new google.maps.LatLng({lat: latitude, lng: longitude});
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });

    var marker;
    marker = new google.maps.Marker({
      map: this.map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: myLatLng
    });

    google.maps.event.addListener(marker, 'dragend', (evt) => {
      this.last_latitud = evt.latLng.lat();
      this.last_longitud = evt.latLng.lng();
    });
  }

   //CARGAR PAGINA
   openBrowser(urlObject:any) {
    this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet');
  }

  ionViewWillEnter() {
    this.getAll();
    this.getAllSecond();
  }

  //PERMISO LLAMAR
  permission() {
    console.log('ACCEDIENDO')
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CALL_PHONE)
    .then(
      result => console.log('Has permission?',result.hasPermission),
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CALL_PHONE)
    ).catch(error => {
      console.log(error)
    });
    
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CALL_PHONE, this.androidPermissions.PERMISSION.CALL_PHONE]);
  }

}