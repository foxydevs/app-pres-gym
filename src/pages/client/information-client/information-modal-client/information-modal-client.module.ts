import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InformationModalClientPage } from './information-modal-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    InformationModalClientPage,
  ],
  imports: [
    IonicPageModule.forChild(InformationModalClientPage),
    PipeModule
  ],
  exports: [
    InformationModalClientPage
  ]
})
export class InformationModalClientPageModule {}