import { Component, Renderer } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { InformationService } from '../../../../app/service/information.service';
import { UsersService } from '../../../../app/service/users.service';
import { ScheduleService } from '../../../../app/service/schedule.service';
import { path } from '../../../../app/config.module';
import { ViewController } from '../../../../../node_modules/ionic-angular/navigation/view-controller';

@IonicPage()
@Component({
  selector: 'information-modal-client',
  templateUrl: 'information-modal-client.html'
})
export class InformationModalClientPage {
  //PROPIEDADES
  public data:any = [];
  public schedules:any = [];
  public information:any = [];
  public profilePicture:any;
  public portadaPicture:any;
  public map: any;
  public idUser:any = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  constructor(
    public navCtrl: NavController,
    public userService: UsersService,
    public iab: InAppBrowser,
    public mainService: InformationService,
    public secondService: ScheduleService,
    public viewCtrl: ViewController,
    public renderer: Renderer,
  ) {
    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'my-popup', true);
    this.userService.getSingle(this.idUser)
    .then(response => {
    this.profilePicture = response.picture;
    this.portadaPicture = response.pic3;
    this.data = response;
    }).catch(error => {
        console.clear()
    })
  }

  //CERRAR MODAL
  public closeModal() {
    this.viewCtrl.dismiss('Close');
  }

  //CARGAR USUARIOS
  public getAll(){
    this.mainService.getAllUser(this.idUser)
    .then(response => {
      this.information = [];
      this.information = response;
    }).catch(error => {
      console.clear;
    })
  }

  //CARGAR USUARIOS
  public getAllSecond(){
    this.secondService.getAllUser(this.idUser)
    .then(response => {
      this.schedules = [];
      this.schedules = response;
    }).catch(error => {
      console.clear;
    })
  }

   //CARGAR PAGINA
   openBrowser(urlObject:any) {
    this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet');
  }

  ionViewWillEnter() {
    this.getAll();
    this.getAllSecond();
  }

}