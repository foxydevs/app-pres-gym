import { Component, Renderer } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { InformationService } from '../../../../app/service/information.service';
import { UsersService } from '../../../../app/service/users.service';
import { ScheduleService } from '../../../../app/service/schedule.service';
import { path } from '../../../../app/config.module';
import { ViewController } from '../../../../../node_modules/ionic-angular/navigation/view-controller';

@IonicPage()
@Component({
  selector: 'horary-modal-client',
  templateUrl: 'horary-modal-client.html'
})
export class HoraryModalClientPage {
  //PROPIEDADES
  public schedules:any = [];
  public profilePicture:any;
  public portadaPicture:any;
  public map: any;
  public idUser:any = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  constructor(
    public navCtrl: NavController,
    public userService: UsersService,
    public iab: InAppBrowser,
    public mainService: InformationService,
    public secondService: ScheduleService,
    public viewCtrl: ViewController,
    public renderer: Renderer,
  ) {

  }

  //CERRAR MODAL
  public closeModal() {
    this.viewCtrl.dismiss('Close');
  }

  //CARGAR USUARIOS
  public getAllSecond(){
    this.secondService.getAllUser(this.idUser)
    .then(response => {
      this.schedules = [];
      this.schedules = response;
    }).catch(error => {
      console.clear;
    })
  }

  ionViewWillEnter() {
    this.getAllSecond();
  }

}