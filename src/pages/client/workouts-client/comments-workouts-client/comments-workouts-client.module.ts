import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommentsWorkoutsClientPage } from './comments-workouts-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    CommentsWorkoutsClientPage,
  ],
  imports: [
    IonicPageModule.forChild(CommentsWorkoutsClientPage),
    PipeModule
  ],
  exports: [
    CommentsWorkoutsClientPage
  ]
})
export class CommentsWorkoutsClientPageModule {}