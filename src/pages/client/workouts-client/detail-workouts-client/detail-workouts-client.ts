import { Component } from '@angular/core';
import { NavController, LoadingController, IonicPage} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { WorkoutsService } from '../../../../app/service/workouts.service';
import { UsersService } from '../../../../app/service/users.service';
import { CommentsWorkoutsClientPage } from '../comments-workouts-client/comments-workouts-client';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'detail-workouts-client',
  templateUrl: 'detail-workouts-client.html',
})
export class DetailWorkoutsClientPage {
  //PROPIEDADES
  public parameter:any;
  public users:any[] = [];
  public data:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    public toast: ToastController,
    public iab: InAppBrowser,
    public mainService: WorkoutsService,
    public secondService: UsersService,
  ) {
    this.parameter = this.navParams.get('parameter');
    this.getSingle(this.parameter);
    this.getAllSecond();
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //CARGAR USUARIOS
  public getAllSecond(){
    this.secondService.getAll()
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear;
    })
  }

  //CARGAR PAGINA
  openBrowser(urlObject:any) {
    this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet,toolbarcolor=#E67E22');
  }
  
  public addComment(parameter:any) {
    this.navCtrl.push(CommentsWorkoutsClientPage, { parameter })
  }
}
