import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailWorkoutsClientPage } from './detail-workouts-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    DetailWorkoutsClientPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailWorkoutsClientPage),
    PipeModule
  ],
  exports: [
    DetailWorkoutsClientPage
  ]
})
export class DetailWorkoutsClientPageModule {}