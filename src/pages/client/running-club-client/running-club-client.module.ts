import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RunningClubClientPage } from './running-club-client';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    RunningClubClientPage,
  ],
  imports: [
    IonicPageModule.forChild(RunningClubClientPage),
    PipeModule
  ],
  exports: [
    RunningClubClientPage
  ]
})
export class RunningClubClientPageModule {}