import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, Platform, IonicPage } from 'ionic-angular';
import { EventsService } from '../../../app/service/events.service';
import { path } from '../../../app/config.module';
import { MembershipService } from '../../../app/service/membership.service';


@IonicPage()
@Component({
  selector: 'running-club-client',
  templateUrl: 'running-club-client.html'
})
export class RunningClubClientPage {
  //PROPIEDADES
  public events:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  public baseId:number = path.id;
  nivelMembresia = localStorage.getItem('currentNivelMembresia');
  membresiaClient = localStorage.getItem('currentMembresia');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public eventsService: EventsService,
    public alertCtrl :AlertController,
    public secondService: MembershipService,
    public plt: Platform     
  ) {
    this.secondService.calculateMembership();
    //MEMBRESIA
    if(this.plt.is('android')) {
      if(+this.membresiaClient < +this.nivelMembresia) {
        this.confirmation('Información', 'Adquiere una membresía que mejor se adapte a tus necesidades para poder gozar de los beneficios de ZFIT.');
      }
    }
  }

  //GET
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.eventsService.getAllType(this.baseId, 2).then(response => {
      this.events = response;
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.clear();
    })
  }

  //ABRIR FORMULARIO
  openForm(parameter?:any) {
    this.navCtrl.push('DetailRunningClubClientPage', { parameter })
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll();
      refresher.complete();
    }, 2000);
  }

  //ENTER
  ionViewWillEnter(){
    this.getAll();
  }

}
