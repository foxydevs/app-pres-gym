import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductsClientPage } from './products-client';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    ProductsClientPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductsClientPage),
    PipeModule
  ],
  exports: [
    ProductsClientPage
  ]
})
export class ProductsClientPageModule {}