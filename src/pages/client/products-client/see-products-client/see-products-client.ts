import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage } from 'ionic-angular';
import { ProductsService } from '../../../../app/service/products.service';
import { CategorysService } from '../../../../app/service/categorys.service';
import { UsersService } from '../../../../app/service/users.service';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'see-products-client',
  templateUrl: 'see-products-client.html'
})
export class SeeProductsClientPage implements OnInit {
  public users:any[] = [];
  public comments:any[] = [];
  public product = {
    name: '',
    description : '',
    price: '',
    quantity: '',
    cost : '',
    user_created: '',
    category: '',
    created_at: '',
    update_at: '',
    picture: '',
    picture1: '',
    picture2: '',
    picture3: '',
    presentaciones_varias: [],
    id: '',
    ver: '',
    presentacion: ''
  }
  //public baseId:number = path.id;
  public parameter:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  moneda = localStorage.getItem('currentCurrency');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public productsService: ProductsService,
    public categorysService: CategorysService,
    public usersService: UsersService,
    public loading: LoadingController,
    public modalCtrl: ModalController
  ) {
    this.parameter = this.navParams.get('parameter');
  }

  ngOnInit() {
  }

  public getSingle(idProduct:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.productsService.getSingle(idProduct)
    .then(response => {
    this.product.name = response.name;
      this.product.description = response.description;
      this.product.price = parseFloat(response.price).toFixed(2).toString();
      this.product.quantity = response.quantity;
      //this.product.user_created = this.returnNameUser(response.user_created);
      this.product.created_at = response.created_at;
      this.product.update_at = response.updated_at;
      this.product.picture = response.picture;
      this.product.presentaciones_varias = response.presentaciones_varias;
      this.product.id = response.id;
      this.product.ver = response.ver;
      if(response.presentaciones) {
        this.product.presentacion = response.presentaciones.name;
      }
      if(response.pictures){
        if(response.pictures[0]) {
          this.product.picture1 = response.pictures[0].picture;
        }
        if(response.pictures[1]) {
          this.product.picture2 = response.pictures[1].picture;
        }
        if(response.pictures[2]) {
          this.product.picture3 = response.pictures[2].picture;
        }
      }
      load.dismiss();
    }).catch(error => {
      console.clear;
      load.dismiss();
    })
  }

  public openPage(parameter:any) {
    this.navCtrl.push('SeeProductsClientCommentPage', { parameter });
  }

  //CARGAR COMENTARIOS POR PRODUCTOS
  public getAllComments(id:any) {
    this.comments = [];
    this.productsService.getAllCommentsByProduct(id)
    .then(res => {
      for(let x of res) {
        let comment = {
          comment: x.comment,
          fecha: moment(x.created_at).format('LL'),
          user: x.users.firstname + ' ' + x.users.lastname,
          picture: x.users.picture
        }
        this.comments.push(comment);
      }
      this.comments.reverse();
    }).catch(error => {
      console.clear();
    });
  }

  ionViewWillEnter() {
    this.getSingle(this.parameter);
    this.getAllComments(this.parameter);
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAllComments(this.parameter);
      refresher.complete();
    }, 2000);
  }

}
