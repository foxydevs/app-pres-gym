import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage } from 'ionic-angular';
import { ProductsService } from '../../../../app/service/products.service';
import { SeeProductsClientPage } from './see-products-client';

//import { path } from "./../../../../app/config.module";
import { ViewController } from 'ionic-angular/navigation/view-controller';

@IonicPage()
@Component({
  selector: 'see-products-client-comment',
  templateUrl: 'see-products-client-comment.html'
})
export class SeeProductsClientCommentPage{
  public comment = {
    comment: '',
    product : '',
    user: ''
  }
  public parameter:any;
  //public baseId:number = path.id;
  public btnDisabled:boolean;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public productsService: ProductsService,
    public viewCtrl: ViewController,
    public loading: LoadingController
  ) {
    this.btnDisabled = false;
    this.parameter = this.navParams.get('parameter');
    this.comment.product = this.parameter;
    this.comment.user = localStorage.getItem('currentId');
  }

   //Insertar Datos
   insert(){
    //let id = this.comment.product;
    if(this.comment.comment) {
      this.btnDisabled = true;
      this.productsService.createComment(this.comment)
      .then(response => {
        this.loading.create({
          content: "Registrando Comentario...",
          duration: 2000
        }).present();        
        this.viewCtrl.dismiss();
        console.clear();
      }).catch(error => {
        this.btnDisabled = false;
        console.clear();
      })
    } else {
      this.toast.create({
        message: "Ingrese un comentario.",
        duration: 800
      }).present();
    }
  }

  public returnProducts(parameter:any) {
    this.navCtrl.push(SeeProductsClientPage, { parameter });
  }

}
