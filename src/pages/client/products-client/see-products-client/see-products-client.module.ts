import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeeProductsClientPage } from './see-products-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    SeeProductsClientPage,
  ],
  imports: [
    IonicPageModule.forChild(SeeProductsClientPage),
    PipeModule
  ],
  exports: [
    SeeProductsClientPage
  ]
})
export class SeeProductsClientPageModule {}