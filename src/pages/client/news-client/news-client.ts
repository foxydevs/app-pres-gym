import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, IonicPage, ToastController } from 'ionic-angular';
import { NewsService } from '../../../app/service/news.service';
import { MembershipService } from '../../../app/service/membership.service';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'news-client',
  templateUrl: 'news-client.html'
})
export class NewsClientPage {
  //PROPIEDADES
  public news:any[] = [];
  public search:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  authentication:any = localStorage.getItem('currentAuthentication');
  nivelMembresia = localStorage.getItem('currentNivelMembresia');
  membresiaClient = localStorage.getItem('currentMembresia');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public newsService: NewsService,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public mainService: MembershipService,
    public toast: ToastController
  ) {
    this.mainService.calculateMembership();
  }

  //CARGAR PRODUCTOS
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.newsService.getAll()
    .then(response => {
      this.news = [];
      this.news = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  ionViewWillEnter() {
    this.getAll();
  }

  seeLiveVideo(parameter:any) {
    if(this.authentication == 'NoAuthentication') {
      this.message('Usted debe iniciar sesión.')
    } else {
      this.loading.create({
        content: 'Cargando...',
        duration: 1000
      }).present();
      this.navCtrl.push('SeeNewsClientPage', { parameter })
    }
  }

  //BUSCAR
  public searchTable() {
    var value = this.search.toLowerCase();
    $("#myList ion-card").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll();
      refresher.complete();
    }, 2000);
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 1000
    }).present();
  }

}
