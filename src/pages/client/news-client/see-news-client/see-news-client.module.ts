import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeeNewsClientPage } from './see-news-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    SeeNewsClientPage,
  ],
  imports: [
    IonicPageModule.forChild(SeeNewsClientPage),
    PipeModule    
  ],
  exports: [
    SeeNewsClientPage
  ]
})
export class SeeNewsClientPageModule {}