import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewsClientPage } from './news-client';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    NewsClientPage,
  ],
  imports: [
    IonicPageModule.forChild(NewsClientPage),
    PipeModule
  ],
  exports: [
    NewsClientPage
  ]
})
export class NewsClientPageModule {}