import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage } from 'ionic-angular';
import { UsersService } from '../../../../app/service/users.service';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { TipsService } from '../../../../app/service/tips.service';
import { InAppBrowser } from '../../../../../node_modules/@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'see-tips-client',
  templateUrl: 'see-tips-client.html'
})
export class SeeTipsClientPage implements OnInit {
  private users:any[] = [];
  private comments:any[] = [];
  private tip = {
    title: '',
    description : '',
    picture: '',
    link: '',
    state: '',
    id: ''
  }
  private parameter:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public tipService: TipsService,
    public iab: InAppBrowser,
    public usersService: UsersService,
    public loading: LoadingController,
    public modalCtrl: ModalController
  ) {
    this.loadAllUsers();
    this.parameter = this.navParams.get('parameter');
  }

  ngOnInit() {
  }

  //CARGAR USUARIO
  public loadAllUsers(){
    this.usersService.getAll()
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear;
    })
  }

  //CARGAR TIP
  public getSingle(idProduct:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.tipService.getSingle(idProduct)
    .then(response => {
      this.tip = response;
      load.dismiss();
    }).catch(error => {
      console.clear;
    })
  }

  //ABRIR PAGINA
  public openPage(parameter:any) {
    this.navCtrl.push('SeeTipsClientCommentPage', { parameter });
  }

  //CARGAR COMENTARIOS POR PRODUCTOS
  public loadCommentsByTip(id:any) {    
    this.tipService.getAllCommentsByTip(id)
    .then(res => {
      this.comments = [];
      for(let x of res) {
        let comment = {
          comment: x.comment,
          fecha: x.created_at,
          user: this.returnNameUser(x.user),
          picture: this.returnPicture(x.user)
        }
        this.comments.push(comment);
      }
      this.comments.reverse();
    }).catch(error => {
      console.clear();
    });
  }

  //RETORNAR EL NOMBRE DE USUARIO
  public returnNameUser(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].firstname + " " + this.users[i].lastname;
      }
    }
  }

  //RETORNAR EL PERFIL DE USUARIO
   public returnPicture(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].picture;
      }
    }
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.getSingle(this.parameter);
      this.loadCommentsByTip(this.parameter);
    }, 1000);
  }

  //OPEN BROWSER
  openBrowser(urlObject:any) {
    this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet');
  }

}
