import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeeTipsClientPage } from './see-tips-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    SeeTipsClientPage,
  ],
  imports: [
    IonicPageModule.forChild(SeeTipsClientPage),
    PipeModule
  ],
  exports: [
    SeeTipsClientPage
  ]
})
export class SeeTipsClientPageModule {}