import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, Platform, IonicPage } from 'ionic-angular';
import { TipsService } from '../../../app/service/tips.service';
import { path } from '../../../app/config.module';
import { MembershipService } from '../../../app/service/membership.service';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'tips-client',
  templateUrl: 'tips-client.html',
})
export class TipsClientPage {
  //PROPIEDADES
  private news:any[] = [];
  private search:any;
  private baseId:number = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  nivelMembresia = localStorage.getItem('currentNivelMembresia');
  membresiaClient = localStorage.getItem('currentMembresia');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public mainService: TipsService,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public secondService: MembershipService,
    public plt: Platform
  ) {
    this.secondService.calculateMembership();
    //MEMBRESIA
    if(this.plt.is('android')) {
      if(+this.membresiaClient < +this.nivelMembresia) {
        this.confirmation('Información', 'Adquiere tu membresía FULL + Nutrición para poder gozar de todos los beneficios de ZFIT.');
      }
    }
  }

  //CARGAR PRODUCTOS
  public getAll() {
    let id = this.baseId;
    this.mainService.getAllUser(id)
    .then(response => {
      this.news = [];
      this.news = response;
    }).catch(error => {
      console.clear
    })
  }

  //BUSCAR
  public searchTable() {
    var value = this.search.toLowerCase();
    $("#myList ion-card").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar la noticia?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.mainService.delete(id)
            .then(response => {
              this.loading.create({
                content: "Eliminando Noticia...",
                duration: 2000
              }).present();
              this.getAll();
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  public detailTip(parameter:any) {
    this.navCtrl.push('SeeTipsClientPage', { parameter })   
  }

  ionViewWillEnter() {
    this.getAll();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll();
      refresher.complete();
    }, 2000);
  }

}
