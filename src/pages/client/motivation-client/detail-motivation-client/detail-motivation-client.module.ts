import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailMotivationsUserPage } from './detail-motivation-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    DetailMotivationsUserPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailMotivationsUserPage),
    PipeModule
  ],
  exports: [
    DetailMotivationsUserPage
  ]
})
export class DetailMotivationsUserPageModule {}