import { Component } from '@angular/core';
import { NavController, LoadingController, Platform, IonicPage} from 'ionic-angular';
import { path } from '../../../app/config.module';
import { MotivationService } from '../../../app/service/motivation.service';
import { AlertController } from '../../../../node_modules/ionic-angular/components/alert/alert-controller';
import { MembershipService } from '../../../app/service/membership.service';

@IonicPage()
@Component({
  selector: 'motivation-client',
  templateUrl: 'motivation-client.html',
})
export class MotivationClientPage {
  //PROPIEDADES
  public table:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  nivelMembresia = localStorage.getItem('currentNivelMembresia');
  membresiaClient = localStorage.getItem('currentMembresia');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public mainService: MotivationService,
    public secondService: MembershipService,
    public plt: Platform
  ) {
    this.secondService.calculateMembership();
    //MEMBRESIA
    if(this.plt.is('android')) {
      if(+this.membresiaClient < +this.nivelMembresia) {
        this.confirmation('Información', 'Adquiere una membresía que mejor se adapte a tus necesidades para poder gozar de los beneficios de ZFIT.');
      }
    }
  }

  openForm(parameter?:any) {
    this.navCtrl.push('DetailMotivationsUserPage', { parameter });
  }

  //CARGAR LOS RETOS
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllUser(path.id)
    .then(response => {
      this.table = []
      this.table = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewWillEnter() {
    this.getAll();
  }

}
