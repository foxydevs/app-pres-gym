import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { UsersService } from '../../../app/service/users.service';
import { LoadingController } from '../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { MembershipService } from '../../../app/service/membership.service';
import { EncriptationService } from '../../../app/service/encriptation.service';

@IonicPage()
@Component({
  selector: 'my-membership-client',
  templateUrl: 'my-membership-client.html'
})
export class MyMembershipClientPage {
  //PROPIEDADES
  profilePicture:any;
  user:any;
  email:any;
  data:any[] = [];
  data2:any[] = [];
  months:any[] = [];
  fechaInicio:any;
  fechaFin:any;
  //BAR CODE PROPIEDADES
  value:any;
  desvalue:any;
  encvalue:any;
  elementType = 'svg';
  format = 'CODE128';
  lineColor = '#000000';
  width = 2;
  height = 90;
  displayValue = true;
  fontOptions = '';
  font = 'monospace';
  textAlign = 'center';
  textPosition = 'bottom';
  textMargin = 2;
  fontSize = 20;
  background = '#ffffff';
  margin = 10;
  marginTop = 10;
  marginBottom = 10;
  marginLeft = 10;
  marginRight = 10;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  idUser:any = localStorage.getItem('currentId');
  typeMembership:any = localStorage.getItem('currentTypeMembership');

  constructor(
    public navCtrl: NavController,
    public mainService: UsersService,
    public secondService: MembershipService,
    public thirdService: EncriptationService,
    public loading: LoadingController
  ) {
    this.secondService.calculateMembership();    
    this.getMonths();
    this.profilePicture = localStorage.getItem('currentPicture');
    this.user = localStorage.getItem("currentFirstName") + ' ' + localStorage.getItem("currentLastName");
    this.email = localStorage.getItem('currentEmail');
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      console.log(response)
      this.data = response;
      if(response.opcion18) {
        this.filterByCode(response.opcion18)
      }      
      var sMyInput = response.id.toString();
      //ENCRIPTAR
      var aMyUTF8Input = this.thirdService.strToUTF8Arr(sMyInput);
      var sMyBase64 = this.thirdService.base64EncArr(aMyUTF8Input);
      this.value = sMyBase64;
      this.fechaInicio = this.returnDate(response.inicioMembresia);
      this.fechaFin = this.returnDate(response.finMembresia);
      localStorage.setItem('currentMembresia', response.tipoNivel);
      localStorage.setItem('currentMembresiaFin', response.finMembresia)
      load.dismiss();
    }).catch(error => {
      console.log(error);
      load.dismiss();
    });
  }

  filterByCode(parameter:any) {
    this.mainService.findCommerce(parameter)
    .then(response => {
      if(response.length > 0) {
        this.data2 = response[0];        
      } else {
        this.updateClient(this.idUser);
      }
      console.log(response)
    }).catch(error => {
      console.log(error)
    });
  }

  public updateClient(idUser:any) {
    let data = {
      inicioMembresia: '',
      finMembresia: '',
      tipoNivel: 0,
      opcion14: 0,
      opcion13: 0,
      opcion15: null,
      opcion18: null,
      id: idUser
    }
    this.mainService.update(data)
    .then(response => {
      this.data = response;
      var sMyInput = response.id.toString();
      //ENCRIPTAR
      var aMyUTF8Input = this.thirdService.strToUTF8Arr(sMyInput);
      var sMyBase64 = this.thirdService.base64EncArr(aMyUTF8Input);
      this.value = sMyBase64;
      this.fechaInicio = this.returnDate(response.inicioMembresia);
      this.fechaFin = this.returnDate(response.finMembresia);
      localStorage.setItem('currentMembresia', response.tipoNivel);
      localStorage.setItem('currentMembresiaFin', response.finMembresia)
    }).catch(error => {
      console.log(error);
    })
  }

  //CARGAR MESES
  public getMonths() {
    this.months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
      "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
    ];
  }

  //CAMBIAR EL FORMATO DE LA FECHA Y HORA//CAMBIAR FORMATO DE FECHA
  public returnDate(fechaJSON:string) {
    var date = new Date(fechaJSON);
    var created_at = date.getDate() + ' ' + this.months[(date.getMonth())] + ', ' + date.getFullYear();
    return created_at;
  }

  ionViewWillEnter() {
    this.getSingle(this.idUser)
    this.profilePicture = localStorage.getItem("currentPicture");
    this.user = localStorage.getItem("currentFirstName") + ' ' + localStorage.getItem("currentLastName");
    this.email = localStorage.getItem('currentEmail');
  }

}