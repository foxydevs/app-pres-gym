import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommentZFitClubClientPage } from './comment-zfit-club-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    CommentZFitClubClientPage,
  ],
  imports: [
    IonicPageModule.forChild(CommentZFitClubClientPage),
    PipeModule
  ],
  exports: [
    CommentZFitClubClientPage
  ]
})
export class CommentZFitClubClientPageModule {}