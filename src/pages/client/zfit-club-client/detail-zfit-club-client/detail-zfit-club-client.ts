import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage, ActionSheetController } from 'ionic-angular';
import { EventsService } from '../../../../app/service/events.service';
import { UsersService } from '../../../../app/service/users.service';
import { Geolocation } from '@ionic-native/geolocation';
import { UserEventsService } from '../../../../app/service/userevents.service';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { SocialSharing } from '../../../../../node_modules/@ionic-native/social-sharing';

declare var google;

@IonicPage()
@Component({
  selector: 'detail-zfit-club-client',
  templateUrl: 'detail-zfit-club-client.html'
})
export class DetailZFitClubClientPage implements OnInit {
  //PROPIEDADES
  private users:any[] = [];
  private comments:any[] = [];
  private months:any[] = [];
  private map: any;
  private userEvents:any[] = [];
  private event = {
    description: '',
    address: '',
    place: '',
    longitude:0,
    latitude:0,
    assistants:0,
    interested:0,
    time:0,
    date: '',
    user: '',
    id: '',
    state: '',
    picture: '',
    tipo: '',
  }
  private parameter:any;
  private changeAssistants:boolean = false;
  private changeInterested:boolean = false;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public eventsService: EventsService,
    public usersService: UsersService,
    public loading: LoadingController,
    public geolocation: Geolocation,
    public usereventsService: UserEventsService,
    public alertCtrl: AlertController,
    public socialSharing: SocialSharing,
    public actionSheetCtrl: ActionSheetController
  ) {
    this.loadAllUsers();
    this.loadAllEvent();
    this.parameter = this.navParams.get('parameter');
  }

  ngOnInit() {
    this.getMonths();
  }

  //CARGAR MESES
  public getMonths() {
    this.months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
      "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
    ];
  }

  //CAMBIAR EL FORMATO DE LA FECHA Y HORA//CAMBIAR FORMATO DE FECHA
  public returnDate(fechaJSON:any) {
    var date = new Date(fechaJSON),
    addTime = 1 * 86400; //Tiempo en segundos
    date.setSeconds(addTime);
    var created_at = date.getDate() + ' ' + this.months[(date.getMonth())] + ', ' + date.getFullYear();
    return created_at;
  }

  //CARGAR COMENTARIOS POR EVENTOS
  public loadCommentsByEvent(id:any) {
    this.comments = [];
    this.eventsService.getAllCommentsByEvent(id)
    .then(res => {
      for(let x of res) {
        let comment = {
          comment: x.comment,
          fecha: x.created_at,
          user: this.returnNameUser(x.user),
          picture: this.returnPicture(x.user)
        }
        this.comments.push(comment);
      }
      this.comments.reverse();
    }).catch(error => {
      console.clear();
    });
  }

  //COMPARTIR
  socialShare() {
    let subject = this.event.description;
    let ios;
    let android;
    if(localStorage.getItem('currentIOS')!='0') {
      ios = ' DESCARGA IOS: ' + localStorage.getItem('currentIOS');
    }
    if(localStorage.getItem('currentAndroid')!='0') {
      android = ' DESCARGA ANDROID: ' + localStorage.getItem('currentAndroid');      
    }
    let message = subject + ', LUGAR: ' + this.event.place + ', DIRECCIÓN: ' +this.event.address + 
    ', FECHA: ' + this.event.date + ', HORA:' + this.event.time + ios + android; 
    let image = this.event.picture;

    let actionSheet = this.actionSheetCtrl.create({
      title: 'Compartir Evento',
      buttons: [
        {
          text: 'Facebook',
          icon: 'logo-facebook',
          handler: () => {
            this.socialSharing.shareViaFacebook(message, image)
          }
        },
        {
          text: 'Instagram',
          icon: 'logo-instagram',
          handler: () => {
            this.socialSharing.shareViaInstagram(message, image)
          }
        },
        {
          text: 'WhatsApp',
          icon: 'logo-whatsapp',
          handler: () => {
            this.socialSharing.shareViaWhatsApp(message, image)
          }
        },
        {
          text: 'Twitter',
          icon: 'logo-twitter',
          handler: () => {
            this.socialSharing.shareViaTwitter(message, image)
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  //Cargar los Usuarios
  public loadAllUsers(){
    this.usersService.getAll()
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear;
    })
  }

  //Cargar los Eventos por Usuario
  public loadAllEvent(){
    this.usereventsService.getAll()
    .then(response => {
      this.userEvents = response;
    }).catch(error => {
      console.clear;
    })
  }

  public loadMap(lat:any, long:any){
  let latitude = lat;
  let longitude = long;
  
    let mapEle: HTMLElement = document.getElementById('map');

    let myLatLng = {lat: latitude, lng: longitude};

    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });

    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      new google.maps.Marker({
        position: myLatLng,
        map: this.map,
        title: 'Hello World!'
      });
      mapEle.classList.add('show-map');
    });
  }

  public loadEvent(idEvent:any) {
      this.eventsService.getSingle(idEvent)
      .then(response => {
        this.event.longitude = response.longitude;
        this.event.latitude = response.latitude;
        this.loadMap(this.event.latitude,this.event.longitude);
        this.event.state = this.returnEventUser(response.id);   
        if(this.event.state == '0') {
          this.changeAssistants = false;
          this.changeInterested = true;
        } else if (this.event.state == '1') {
          this.changeAssistants = true;
          this.changeInterested = false;
        } else {
          this.changeAssistants = false;
          this.changeInterested = false;
        }       
        this.event.description = response.description;  
        this.event.address = response.address;  
        this.event.place = response.place;  
        this.event.date = this.returnDate(response.date);
        this.event.time = response.time;
        this.event.assistants = response.assistants.length;
        this.event.interested = response.interested.length;
        this.event.tipo = response.types.description;
        this.event.picture = response.picture;
        this.event.user = response.user.firstname + ' ' + response.user.lastname;
        this.event.id = response.id;              
      }).catch(error => {
          console.clear;
      })
  }

  //Devolver el Nombre del Usuario
  public returnNameUser(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].firstname + " " + this.users[i].lastname;
      }
    }
  }

  //Devolver el Nombre del Usuario
  public returnPicture(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].picture;
      }
    }
  }

  public returnEventUser(idEvent:any):any {
    let idUser = localStorage.getItem("currentId");
    for(var i = 0;i<this.userEvents.length;i++) {
      if(this.userEvents[i].event == idEvent && this.userEvents[i].user == idUser) {
        return this.userEvents[i].state;
      }
    }
  }

  public openPage(parameter:any) {
    this.navCtrl.push('CommentZFitClubClientPage', { parameter });
  }

  public interested() {
    let interested = {
      state: '0',
      user: localStorage.getItem("currentId"),
      event: this.parameter
    }
    //let id = this.parameter;
    this.usereventsService.create(interested)
    .then(response => {
      this.loading.create({
        content: "Cargando...",
        duration: 1000
      }).present();
      this.loadAllEvent();
      this.changeAssistants = false;
      this.changeInterested = true;
      console.clear;
    }).catch(error => {
      console.clear;
    })
  }

  public assist() {
    let interested = {
      state: '1',
      user: localStorage.getItem("currentId"),
      event: this.parameter
    }
    //let id = this.parameter;
    this.usereventsService.create(interested)
    .then(response => {
      this.loading.create({
        content: "Cargando...",
        duration: 1000
      }).present();
      this.loadAllEvent();
      this.changeAssistants = true;
      this.changeInterested = false;
      console.clear;
    }).catch(error => {
      console.clear;
    })
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.loadEvent(this.parameter);
      this.loadCommentsByEvent(this.parameter)
    }, 800);
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.loadCommentsByEvent(this.parameter);
      refresher.complete();
    }, 2000);
  }

}