import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormZFitClubClientPage } from './form-zfit-club-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormZFitClubClientPage,
  ],
  imports: [
    IonicPageModule.forChild(FormZFitClubClientPage),
    PipeModule
  ],
  exports: [
    FormZFitClubClientPage
  ]
})
export class FormZFitClubClientPageModule {}