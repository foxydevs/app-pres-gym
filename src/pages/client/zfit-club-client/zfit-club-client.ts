import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, Platform, ToastController } from 'ionic-angular';
import { EventsService } from '../../../app/service/events.service';
import { path } from '../../../app/config.module';
import { MembershipService } from '../../../app/service/membership.service';
import { IonicPage } from '../../../../node_modules/ionic-angular/navigation/ionic-page';

@IonicPage()
@Component({
  selector: 'zfit-club-client',
  templateUrl: 'zfit-club-client.html'
})
export class ZFitClubClientPage {
  //PROPIEDADES
  private events:any[] = [];
  private table:any[] = [];
  private idUser:any;
  selectItem:any = 'zfit';
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  nivelMembresia = localStorage.getItem('currentNivelMembresia');
  membresiaClient = localStorage.getItem('currentMembresia');
  authentication:any = localStorage.getItem('currentAuthentication');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public eventsService: EventsService,
    public alertCtrl :AlertController,
    public secondService: MembershipService,
    public toast: ToastController,
    public plt: Platform     
  ) {
    this.idUser = localStorage.getItem("currentId");
    this.secondService.calculateMembership();
    //MEMBRESIA
    if(this.plt.is('android')) {
      if(+this.membresiaClient < +this.nivelMembresia) {
        this.confirmation('Información', 'Adquiere una membresía que mejor se adapte a tus necesidades para poder gozar de los beneficios de ZFIT.');
      }
    }
  }

  //GET
  public getAll() {
    this.selectItem = 'events';
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.eventsService.getAllUser(this.idUser).then(response => {
      this.events = response;
      load.dismiss();
    }).catch(error => {
      console.clear();
    })
  }

  //GET
  public getAllUser() {
    this.selectItem = 'zfit';
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.eventsService.getAllType(path.id, 3).then(response => {
      this.table = response;
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.clear();
    })
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //ABRIR FORMULARIO
  openForm(parameter?:any) {
    if(this.authentication == 'NoAuthentication') {
      this.message('Usted debe iniciar sesión.')
    } else {
      this.navCtrl.push('FormZFitClubClientPage', { parameter })
    }
  }

  //ABRIR FORMULARIO
  detailForm(parameter?:any) {
    if(this.authentication == 'NoAuthentication') {
      this.message('Usted debe iniciar sesión.')
    } else {
      this.navCtrl.push('DetailZFitClubClientPage', { parameter })
    }    
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      if(this.selectItem == 'zfit') {
        this.getAllUser();
      } else if(this.selectItem == 'events') {
        this.getAll();
      }
      refresher.complete();
    }, 2000);
  }

  //ENTER
  ionViewWillEnter(){
    if(this.selectItem == 'zfit') {
      this.getAllUser();
    } else if(this.selectItem == 'events') {
      this.getAll();
    }
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 1000
    }).present();
  }

}
