import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ModalController, Platform, IonicPage } from 'ionic-angular';
import { StaffService } from '../../../app/service/staff.service';
import { AlertController } from '../../../../node_modules/ionic-angular/components/alert/alert-controller';
import { path } from '../../../app/config.module';
import { InAppBrowser } from '../../../../node_modules/@ionic-native/in-app-browser';
import { MembershipService } from '../../../app/service/membership.service';

@IonicPage()
@Component({
  selector: 'staff-client',
  templateUrl: 'staff-client.html'
})
export class StaffClientPage {
  //PROPIEDADES
  public staff:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  selectItem:any = 'users';
  nivelMembresia = localStorage.getItem('currentNivelMembresia');
  membresiaClient = localStorage.getItem('currentMembresia');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loading: LoadingController,
    public modal: ModalController,
    public iab: InAppBrowser,
    public alertCtrl: AlertController,
    public mainService: StaffService,
    public secondService: MembershipService,
    public plt: Platform
  ) {
    this.secondService.calculateMembership();
    //MEMBRESIA
    if(this.plt.is('android')) {
      if(+this.membresiaClient < +this.nivelMembresia) {
        this.confirmation('Información', 'Adquiere una membresía que mejor se adapte a tus necesidades para poder gozar de los beneficios de ZFIT.');
      }
    }
  }

  //CARGAR USUARIOS
  public getAll(){
    let load = this.loading.create({
      content: "Cargando..."
    });
    load.present();
    this.mainService.getAllUser(path.id)
    .then(response => {
      this.staff = [];
      this.staff = response;
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewWillEnter() {
    this.getAll();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll();
      refresher.complete();
    }, 2000);
  }

  //CARGAR PAGINA
  openBrowser(urlObject:any) {
    this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet');
  }

}
