import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyOrdersClientPage } from './my-orders';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    MyOrdersClientPage,
  ],
  imports: [
    IonicPageModule.forChild(MyOrdersClientPage),
    PipeModule
  ],
  exports: [
    MyOrdersClientPage
  ]
})
export class MyOrdersClientPageModule {}