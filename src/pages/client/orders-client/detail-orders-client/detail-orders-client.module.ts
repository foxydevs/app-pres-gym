import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailOrdersClientPage } from './detail-orders-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    DetailOrdersClientPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailOrdersClientPage),
    PipeModule
  ],
  exports: [
    DetailOrdersClientPage
  ]
})
export class DetailOrdersClientPageModule {}