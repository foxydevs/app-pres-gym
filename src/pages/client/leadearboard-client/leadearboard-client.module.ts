import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeadearboardClientPage } from './leadearboard-client';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    LeadearboardClientPage,
  ],
  imports: [
    IonicPageModule.forChild(LeadearboardClientPage),
    PipeModule
  ],
  exports: [
    LeadearboardClientPage
  ]
})
export class LeadearboardClientPageModule {}