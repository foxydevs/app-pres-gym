import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { MotivationService } from '../../../../app/service/motivation.service';
import { path } from '../../../../app/config.module';
import { LoadingController } from '../../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { IonicPage } from '../../../../../node_modules/ionic-angular/navigation/ionic-page';

@IonicPage()
@Component({
  selector: 'detail-motivations-client',
  templateUrl: 'detail-motivations-client.html',
})
export class DetailMotivationUserPage {
  //PROPIEDADES
  public parameter:any;
  selectItem:any = 'actualizar';
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  public data = {
    title: '',
    description: '',
    user: path.id,
    picture: 'https://bpresentacion.s3.us-west-2.amazonaws.com/avatar/8Bjpl7wuGY88gjCBWR3nsBELyEgqEbXwPkyvWiCz.png',
    id: ''
  }

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toast: ToastController,
    public loading: LoadingController,
    public mainService: MotivationService
  ) {
    this.parameter = this.navParams.get('parameter');
    this.getSingle(this.parameter);
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }
}
