import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommentsChallengeClientPage } from './comments-challenges-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    CommentsChallengeClientPage,
  ],
  imports: [
    IonicPageModule.forChild(CommentsChallengeClientPage),
    PipeModule
  ],
  exports: [
    CommentsChallengeClientPage
  ]
})
export class CommentsChallengeClientPageModule {}