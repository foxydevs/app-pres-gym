import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailPromotionClientPage } from './detail-promotion-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    DetailPromotionClientPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailPromotionClientPage),
    PipeModule
  ],
  exports: [
    DetailPromotionClientPage
  ]
})
export class DetailPromotionClientPageModule {}