import { Component } from '@angular/core';
import { NavController, AlertController, IonicPage, ToastController} from 'ionic-angular';
import { LoadingController } from '../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { PromotionService } from '../../../app/service/promotion.service';
import { MembershipService } from '../../../app/service/membership.service';
import { Platform } from '../../../../node_modules/ionic-angular/platform/platform';
import { path } from '../../../app/config.module';
import { DescuentoService } from '../../../app/service/descuentos.service';
import { Clipboard } from '@ionic-native/clipboard';

@IonicPage()
@Component({
  selector: 'promotion-client',
  templateUrl: 'promotion-client.html',
})
export class PromotionClientPage {
  //PROPIEDADES
  public table:any[] = [];
  public idUser:any;
  public idUserApp:any = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  selectItem:any = 'promociones';
  nivelMembresia = localStorage.getItem('currentNivelMembresia');
  membresiaClient = localStorage.getItem('currentMembresia');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public mainService: PromotionService,
    public thirdService: DescuentoService,
    public secondService: MembershipService,
    public plt: Platform,
    public clipboard: Clipboard,
    public toast: ToastController,
  ) {
    this.idUser = localStorage.getItem('currentId');
    this.secondService.calculateMembership();    
    //MEMBRESIA
    if(this.plt.is('android')) {
      if(+this.membresiaClient < +this.nivelMembresia) {
        this.confirmation('Información', 'Adquiere una membresía que mejor se adapte a tus necesidades para poder gozar de los beneficios de ZFIT.');
      }
    }
  }

  openForm(parameter?:any) {
    this.navCtrl.push('DetailPromotionClientPage', { parameter });
  }

  //CARGAR LOS RETOS
  public getAll(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllUser(id)
    .then(response => {
      this.table = []
      this.table = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR LOS RETOS
  public getAll2(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.thirdService.getAllUser(id)
    .then(response => {
      this.table = []
      this.table = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewWillEnter() {
    if(this.selectItem=='promociones') {
      this.getAll(this.idUserApp);
    } else if(this.selectItem=='descuentos') {
      this.getAll2(this.idUserApp);
    }
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      if(this.selectItem=='promociones') {
        this.getAll(this.idUserApp);
      } else if(this.selectItem=='descuentos') {
        this.getAll2(this.idUserApp);
      }
      refresher.complete();
    }, 2000);
  }

  //COPIAR AL PORTAPAPELES
  copyClipboard(parameter:any) {
    this.clipboard.copy(parameter);
    this.message('Código de Descuento copiado al portapapeles.')
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

}
