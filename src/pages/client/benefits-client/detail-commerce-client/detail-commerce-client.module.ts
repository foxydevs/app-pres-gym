import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailCommerceClientPage } from './detail-commerce-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    DetailCommerceClientPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailCommerceClientPage),
    PipeModule
  ],
  exports: [
    DetailCommerceClientPage
  ]
})
export class DetailCommerceClientPageModule {}