import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailBenefitsClientPage } from './detail-benefits-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    DetailBenefitsClientPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailBenefitsClientPage),
    PipeModule
  ],
  exports: [
    DetailBenefitsClientPage
  ]
})
export class DetailBenefitsClientPageModule {}