import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessagesClientPage } from './messages-client';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    MessagesClientPage,
  ],
  imports: [
    IonicPageModule.forChild(MessagesClientPage),
    PipeModule
  ],
  exports: [
    MessagesClientPage
  ]
})
export class MessagesClientPageModule {}