import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SendMessagesClientPage } from './send-messages-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    SendMessagesClientPage,
  ],
  imports: [
    IonicPageModule.forChild(SendMessagesClientPage),
    PipeModule
  ],
  exports: [
    SendMessagesClientPage
  ]
})
export class SendMessagesClientPageModule {}