import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage } from 'ionic-angular';
import { MessagesService } from '../../../../../app/service/messages.service';

@IonicPage()
@Component({
  selector: 'messages-user-send-client',
  templateUrl: 'messages-user-send-client.html'
})
export class SendMessagesUserClientPage{
  public message = {
    subject: '',
    message : '',
    user_send: '',
    user_receipt: '',
    tipo: 0,
  }
  public parameter:any;
  public btnDisabled:boolean;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public messagesService: MessagesService,
    public loading: LoadingController
  ) {
    this.btnDisabled = false;
    this.parameter = this.navParams.get('parameter');
    this.message.user_send = localStorage.getItem('currentId');
    this.message.user_receipt = this.parameter;
    if(localStorage.getItem('currentStaff')) {
      this.message.tipo = 1;
    }
  }

   //Insertar Datos
   public sendMessage(){
    let id = this.parameter;
    this.btnDisabled = true;
    this.messagesService.create(this.message)
    .then(response => {
        this.loading.create({
        content: "Enviando Mensaje...",
        duration: 2000
        }).present();
        this.returnMessages(id)
        console.log(response)
        }).catch(error => {
        this.btnDisabled = false;
        console.log(error)
    })
  }

  public returnMessages(parameter:any) {
    this.navCtrl.push('MessagesUserClientPage', { parameter });
  }

}