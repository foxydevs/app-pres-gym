import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessagesUserClientPage } from './messages-user-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    MessagesUserClientPage,
  ],
  imports: [
    IonicPageModule.forChild(MessagesUserClientPage),
    PipeModule
  ],
  exports: [
    MessagesUserClientPage
  ]
})
export class MessagesUserClientPageModule {}