import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, IonicPage, Platform } from 'ionic-angular';
import { MessagesService } from '../../../../app/service/messages.service';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'messages-user-client',
  templateUrl: 'messages-user-client.html'
})
export class MessagesUserClientPage {
  //PROPIEDADES
  public idClient:any;
  public messages:any[] = [];
  public message = {
    subject: 'Sin Asunto',
    message : '',
    user_send: '',
    user_receipt: '',
    picture: '',
    tipo: 0
  }
  public parameter:any;
  public id:any;
  public idUserSend:any;
  public idUserReceipt:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  //public baseId:number = path.id;

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loading: LoadingController,
    public messagesService: MessagesService,
    private platform: Platform,
    private iab: InAppBrowser
  ) {
    this.parameter = this.navParams.get('parameter');
    this.idClient = localStorage.getItem("currentId");
    this.id = this.parameter;
    this.loadMessages(this.idClient, this.parameter);
    this.idUserSend = localStorage.getItem('currentId');
    this.idUserReceipt = this.parameter;
    this.message.user_send = localStorage.getItem('currentId');
    this.message.user_receipt = this.parameter;
    if(localStorage.getItem('currentStaff')) {
      this.message.tipo = 1;
    }
  }

  //CARGAR MENSAJES
  public loadMessages(user_send:any, user_receipt:any) {
    this.messagesService.getAll()
    .then(response => {
      this.messages = [];
      for(let x of response) {
        if(x.user_send == user_send && x.user_receipt == user_receipt
          || (x.user_send == user_receipt && x.user_receipt == user_send)) {
          this.messages.push(x);
        }
      }
      this.messages.reverse();
    }).catch(error => {
        console.clear
    })
  }

  //ENVIAR MENSAJES
  public sendMessage(parameter:any) {
    this.navCtrl.push('SendMessagesClientPage', { parameter });
  }

  ionViewWillEnter() {
    this.loadMessages(this.idClient, this.parameter);
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.loadMessages(this.idClient, this.parameter);
      refresher.complete();
    }, 2000);
  }

  //AGREGAR
  sendMessages() {
    console.log(this.message)
    this.messagesService.create(this.message)
    .then(response => {
      console.log(response);
      this.message.message = '';
      this.loadMessages(this.idClient, this.parameter);
    }).catch(error => {
      console.clear
    });
  }

  //CARGAR PAGINA
  openDocument(urlObject:any, title:any) {
    if (this.platform.is('android')) {
      this.iab.create('http://docs.google.com/gview?url=' + urlObject, '_blank', 'presentationstyle=pagesheet,toolbarcolor=#003D6E');      
    } else {
      this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet');
    }
  }

  //CARGAR PAGINA
  openBrowser(urlObject:any) {
    this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet,toolbarcolor=#E67E22');
  }

}
