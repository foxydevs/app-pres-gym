import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { SocialService } from '../../../../app/service/social.service';

@IonicPage()
@Component({
  selector: 'comment-social-client',
  templateUrl: 'comment-social-client.html'
})
export class CommentSocialClientPage{
  public data = {
    comment: '',
    social : '',
    user: ''
  }
  public parameter:any;
  public btnDisabled:boolean;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public mainService: SocialService,
    public viewCtrl: ViewController,
    public loading: LoadingController
  ) {
    this.btnDisabled = false;
    this.parameter = this.navParams.get('parameter');
    this.data.social = this.parameter;
    this.data.user = localStorage.getItem('currentId');
  }

  //CERRAR MODAL
  public closeModal() {
    this.viewCtrl.dismiss('Close');
  }

   //AGREGAR
   create(){
       console.log(this.data)
    if(this.data.comment) {
      this.btnDisabled = true;
      let load = this.loading.create({
        content: "Registrando Comentario..."
      });
      load.present();   
      this.mainService.createComment(this.data)
      .then(response => {
        this.viewCtrl.dismiss();
        load.dismiss();
      }).catch(error => {
        this.btnDisabled = false;
        load.dismiss();
      })
    } else {
      this.toast.create({
        message: "Ingrese un comentario.",
        duration: 800
      }).present();
    }
  }

}
