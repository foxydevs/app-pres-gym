import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProgressUserPage } from './progress-user';
 
@NgModule({
  declarations: [
    ProgressUserPage,
  ],
  imports: [
    IonicPageModule.forChild(ProgressUserPage),
  ],
  exports: [
    ProgressUserPage
  ]
})
export class ProgressUserPageModule {}