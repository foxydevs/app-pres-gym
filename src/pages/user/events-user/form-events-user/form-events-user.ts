import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, AlertController, IonicPage, ActionSheetController } from 'ionic-angular';
import { EventsService } from '../../../../app/service/events.service';
import { CategorysService } from '../../../../app/service/categorys.service';
import { UsersService } from '../../../../app/service/users.service';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { path } from '../../../../app/config.module';
import { SocialSharing } from '@ionic-native/social-sharing';
import * as moment from 'moment';

//JQUERY
declare var $:any;
declare var google;

@IonicPage()
@Component({
  selector: 'form-events-user',
  templateUrl: 'form-events-user.html'
})
export class FormEventsUserPage{
  //PROPIEDADES
  public map: any;
  public comments:any[] = [];
  public data = {
    address: '',
    description : '',
    date: '',
    time: '',
    latitude: '',
    longitude: '',
    user_created: +localStorage.getItem('currentId'),
    user_owner: +localStorage.getItem('currentId'),
    place: '',
    tipo: 1,
    place_id:'0',
    id: '',
  }
  public events = {
    picture : localStorage.getItem('currentPicture'),
    assistants: '',
    interested: '',
  }
  public title:any;
  public marker:any;
  public parameter:any;
  public btnDisabled:boolean = false;
  public basePath:string = path.path;
  public baseIdUser:string = path.id;
  selectItem:any = 'actualizar';
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public mainService: EventsService,
    public categorysService: CategorysService,
    public usersService: UsersService,
    public loading: LoadingController,
    public geolocation: Geolocation,
    public view: ViewController,
    public alertCtrl: AlertController,
    public socialSharing: SocialSharing,
    public actionSheetCtrl: ActionSheetController
  ) {
    this.parameter = this.navParams.get('parameter');
    if(this.parameter) {
      this.title = "Edición de Evento";
      this.getSingle(this.parameter);
      this.getAllComments(this.parameter);
    } else {
      this.getPosition();
      this.title = "Nuevo Evento";
    }
  }

  //CARGAR EVENTO
  public getSingle(parameter:any) {
    this.mainService.getSingle(parameter)
    .then(res => {     
      this.data.latitude = res.latitude;
      this.data.longitude = res.longitude;
      this.loadMapUpdate(this.data.latitude, this.data.longitude);
      this.events.picture = res.picture;
      this.data.place = res.place;
      this.data.address = res.address;
      this.data.description = res.description;
      this.data.date = res.date;
      this.data.time = res.time;
      this.data.place_id = res.place_id;
      this.events.assistants = res.assistants.length;
      this.events.interested = res.interested.length;
      this.data.id = res.id;
    }).catch(error => {
      console.log(error)
    });
  }

  getPosition():any{
    this.geolocation.getCurrentPosition().then(resp => {
      this.loadMap(resp)
     }).catch(error => {
      console.log('Error getting location', error);
    });
  }

  //COMPARTIR
  shareSocial() {
    let subject = this.data.description;
    let ios;
    let android;
    if(localStorage.getItem('currentIOS')!='0') {
      ios = ' DESCARGA IOS: ' + localStorage.getItem('currentIOS');
    }
    if(localStorage.getItem('currentAndroid')!='0') {
      android = ' DESCARGA ANDROID: ' + localStorage.getItem('currentAndroid');      
    }
    let message = 'LUGAR: ' + this.data.place + ', DIRECCIÓN: ' +this.data.address + 
    ', FECHA: ' + this.data.date + ', HORA:' + this.data.time + ios + android; 
    let file = this.events.picture;
    this.socialSharing.share(message, subject, file);
  }

  //CARGAR MAPA
  public loadMap(position: Geoposition) {
  let latitude = position.coords.latitude;
  let longitude = position.coords.longitude;
  this.data.latitude = latitude.toString();
  this.data.longitude = longitude.toString();
    let mapEle: HTMLElement = document.getElementById('map');
    let myLatLng = new google.maps.LatLng({lat: latitude, lng: longitude});
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });

    var marker;
    marker = new google.maps.Marker({
      map: this.map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: myLatLng
    });

    google.maps.event.addListener(marker, 'dragend', (evt) => {
      this.data.latitude = evt.latLng.lat();
      this.data.longitude = evt.latLng.lng();
    });
  }

  //CARGAR MAPA ACTUALIZAR
  public loadMapUpdate(lat:any, lon:any) {
  let latitude = lat;
  let longitude = lon;
  this.data.latitude = latitude.toString();
  this.data.longitude = longitude.toString();
    let mapEle: HTMLElement = document.getElementById('map');
    let myLatLng = new google.maps.LatLng({lat: latitude, lng: longitude});
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });

    var marker;
    marker = new google.maps.Marker({
      map: this.map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: myLatLng
    });

    google.maps.event.addListener(marker, 'dragend', (evt) => {
      this.data.latitude = evt.latLng.lat();
      this.data.longitude = evt.latLng.lng();
    });
  }

  toggleBounce() {
    this.marker.getPosition()
    console.log(this.marker.getPosition())
    if (this.marker.getAnimation()) {
      this.marker.setAnimation(null);
      console.log(this.marker)
    } else {
      this.marker.setAnimation(google.maps.Animation.BOUNCE);
      console.log(this.marker)
    }
  }

  //GUARDAR CAMBIOS
  public saveChanges() {
    if(this.data.place) {
      if(this.data.address) {
        if(this.data.description) {
          if(this.data.description.length >= 10) {
            if(this.data.date) {
              if(this.data.time) {
                this.btnDisabled = true;
                if(this.parameter) {
                  console.log(this.data)
                  this.loading.create({
                    content: "Actualizando...",
                    duration: 1500
                  }).present();
                  this.mainService.update(this.data)
                  .then(response => {
                  this.parameter = response.id;
                  this.data.id = response.id;
                  this.confirmation('Evento Actualizado', 'El evento fue actualizado exitosamente.');
                  this.btnDisabled = false;
                  }).catch(error => {
                    this.btnDisabled = false;
                    console.log(error)
                  })
                } else {
                  console.log(this.data)
                  this.loading.create({
                    content: "Agregando...",
                    duration: 1500
                  }).present();
                  this.mainService.create(this.data)
                  .then(response => {
                  this.parameter = response.id;
                  this.data.id = response.id;
                  this.confirmation('Evento Agregado', 'El evento fue agregado exitosamente.');
                  this.btnDisabled = false;
                  }).catch(error => {
                    this.btnDisabled = false;
                    console.log(error)
                  })
                }
              } else {
                this.message('La hora es requerida.');
              }
            } else {
              this.message('La fecha es requerida.');
            }
          } else {
            this.message('La descripcion debe de llevar al menos 10 caracteres.');
          }
        } else {
          this.message('La descripción es requerida.');
        }
      } else {
        this.message('La dirección es requerida.');
      }
    } else {
      this.message('El lugar es requerido.');
    }
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

  //Subir Imagenes de Producto
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}event/upload/${this.data.id}`;
    
    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0]
          },
          function(respuesta) {
            $('#imgAvatar').attr("src", respuesta.picture)
            $("#"+id).val('')
          }
        );
      } else {
        this.message('La imagen es demasiado grande.');
      }
    } else {
      this.message('El tipo de imagen no es válido.');
    }
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar el evento?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando..."
            });
            load.present();
            this.mainService.delete(id)
            .then(response => {
              load.dismiss();
              this.view.dismiss(response)
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  public confirmation(title: any, message?:any) {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss('Close');
  }

  //CARGAR COMENTARIOS POR EVENTOS
  public getAllComments(id:any) {
    this.comments = [];
    this.mainService.getAllCommentsByEvent(id)
    .then(res => {
      for(let x of res) {
        let comment = {
          comment: x.comment,
          fecha: moment(x.created_at).format('LL'),
          user: x.users.firstname + ' ' + x.users.lastname,
          picture: x.users.picture
        }
        this.comments.push(comment);
      }
      this.comments.reverse();
    }).catch(error => {
      console.clear();
    });
  }

  //COMPARTIR
  socialShare() {
    let subject = this.data.description;
    let ios;
    let android;
    if(localStorage.getItem('currentIOS')!='0') {
      ios = ' DESCARGA IOS: ' + localStorage.getItem('currentIOS');
    }
    if(localStorage.getItem('currentAndroid')!='0') {
      android = ' DESCARGA ANDROID: ' + localStorage.getItem('currentAndroid');      
    }
    let message = subject + ', LUGAR: ' + this.data.place + ', DIRECCIÓN: ' +this.data.address + 
    ', FECHA: ' + this.data.date + ', HORA:' + this.data.time + ios + android; 
    let image = this.events.picture;

    let actionSheet = this.actionSheetCtrl.create({
      title: 'Compartir Evento',
      buttons: [
        {
          text: 'Facebook',
          icon: 'logo-facebook',
          handler: () => {
            this.socialSharing.shareViaFacebook(message, image)
          }
        },
        {
          text: 'Instagram',
          icon: 'logo-instagram',
          handler: () => {
            this.socialSharing.shareViaInstagram(message, image)
          }
        },
        {
          text: 'WhatsApp',
          icon: 'logo-whatsapp',
          handler: () => {
            this.socialSharing.shareViaWhatsApp(message, image)
          }
        },
        {
          text: 'Twitter',
          icon: 'logo-twitter',
          handler: () => {
            this.socialSharing.shareViaTwitter(message, image)
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

}
