import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InformationUserPage } from './information-user';
 
@NgModule({
  declarations: [
    InformationUserPage,
  ],
  imports: [
    IonicPageModule.forChild(InformationUserPage),
  ],
  exports: [
    InformationUserPage
  ]
})
export class InformationUserPageModule {}