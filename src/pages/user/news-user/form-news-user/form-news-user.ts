import { Component } from '@angular/core';
import { NavController, ViewController, LoadingController, IonicPage } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { NewsService } from '../../../../app/service/news.service';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { path } from '../../../../app/config.module';
import { InAppBrowser } from '@ionic-native/in-app-browser';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'form-news-user',
  templateUrl: 'form-news-user.html'
})
export class FormNewsUserPage {
  private title:any;
  private parameter:any;
  private btnDisabled:boolean = false;
  private data = {
    id: '',
    title: '',
    description: '',
    link: '',
    user: localStorage.getItem('currentId'),
    video: '',
    picture: localStorage.getItem('currentPicture'),
  }
  private basePath:string = path.path;
  selectItem:any = 'actualizar';
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(public navCtrl: NavController,
  public loading:LoadingController,
  public navParams: NavParams,
  public toast: ToastController,
  public view: ViewController,
  public alertCtrl: AlertController,
  public iab: InAppBrowser,
  public mainService: NewsService,) {
    this.parameter = this.navParams.get('parameter')
    if(this.parameter) {
      this.title = 'Edición de Datos'
      this.loadNew(this.parameter);
    } else {
      this.title = 'Ingreso de Datos';
    }    
  }

  //CARGAR
  public loadNew(id:any) {
    this.mainService.getSingle(id)
    .then(res => {
      this.data.title = res.title;
      this.data.description = res.description;
      this.data.link = res.link;
      this.data.video = res.video;
      this.data.picture = res.picture;
      this.data.id = res.id;
    }).catch(error => {
      console.log(error)
    })
  }

  //INSERTAR DATOS
  public saveChanges() {
    let link = this.data.video;
    let codigo = link.replace('https://www.youtube.com/watch?v=', '');
    this.data.video = 'https://www.youtube.com/embed/' + codigo;
    this.data.picture = $('img[alt="Avatar"]').attr('src');
  
    if(this.data.title) {
      if(this.data.description) {
        this.btnDisabled = true;
        if(this.parameter) {
          this.update(this.data)
        } else {
          this.create(this.data)
        }
      } else {
        this.message('La descripción es requerida.')
      }
    } else {
      this.message('El título es requerido.')  
    }
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .then(response => {
      this.confirmation('Noticia Agregada', 'La noticia fue agregada exitosamente.');
      this.parameter = response.id;
      this.data.id = response.id;
      this.btnDisabled = false;
    }).catch(error => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .then(response => {
      this.confirmation('Noticia Actualizada', 'La noticia fue actualizada exitosamente.');
      this.closeModal();
      console.log(response);
      this.btnDisabled = false;
    }).catch(error => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //MENSAJE DE CONFIRMACION
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //IMAGEN
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    console.log(archivos[0])
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'news'
          },
          function(respuesta) {
            $('#imgAvatar').attr("src", respuesta.url)
            $("#"+id).val('')
          }
        );
      } else {
        this.message('La imagen es demasiado grande.')
      }
    } else {
      this.message('El tipo de imagen no es válido.')
    }
  }

  //ELIMINAR
  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar la noticia?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando..."
            });
            load.present();
            this.mainService.delete(id)
            .then(response => {
              load.dismiss();
              this.closeModal();
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

  //CARGAR PAGINA
  openBrowser(urlObject:any) {
    this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet,toolbarcolor=#E67E22');
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss('Close');
  }

}
