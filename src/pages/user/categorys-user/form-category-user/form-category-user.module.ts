import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoryFormUserPage } from './form-category-user';
import { PipeModule } from '../../../../pipes/pipes.module';
import { AndroidPermissions } from '../../../../../node_modules/@ionic-native/android-permissions';
 
@NgModule({
  declarations: [
    CategoryFormUserPage,
  ],
  imports: [
    IonicPageModule.forChild(CategoryFormUserPage),
    PipeModule
  ],
  exports: [
    CategoryFormUserPage
  ],
  providers: [
    AndroidPermissions
  ]
})
export class CategoryFormUserPageModule {}