import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategorysUserPage } from './categorys-user';
 
@NgModule({
  declarations: [
    CategorysUserPage,
  ],
  imports: [
    IonicPageModule.forChild(CategorysUserPage),
  ],
  exports: [
    CategorysUserPage
  ]
})
export class CategorysUserPageModule {}