import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormStaffUserPage } from './form-staff-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormStaffUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormStaffUserPage),
    PipeModule
  ],
  exports: [
    FormStaffUserPage
  ]
})
export class FormStaffUserPageModule {}