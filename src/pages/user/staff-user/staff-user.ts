import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ModalController, IonicPage } from 'ionic-angular';
import { UsersService } from '../../../app/service/users.service';
import { StaffService } from '../../../app/service/staff.service';
import { AlertController } from '../../../../node_modules/ionic-angular/components/alert/alert-controller';
import { path } from '../../../app/config.module';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'staff-user',
  templateUrl: 'staff-user.html'
})
export class StaffUserPage {
  //PROPIEDADES
  private idUser:any = path.id;
  private search:any;
  private users:any[] = [];
  private staff:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  selectItem:any = 'users';
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loading: LoadingController,
    public modal: ModalController,
    public alertCtrl: AlertController,
    public mainService: UsersService,
    public secondService: StaffService,
  ) {
  }

  //CARGAR USUARIOS
  public getAll(){
    let load = this.loading.create({
      content: "Cargando..."
    });
    load.present();
    this.mainService.getClients(this.idUser)
    .then(response => {
      this.users = [];
      this.users = response;
      load.dismiss();
    }).catch(error => {
      console.clear
      load.dismiss();
    })
  }

  //CARGAR STAFF
  public getAllSecond(){
    let load = this.loading.create({
      content: "Cargando..."
    });
    load.present();
    this.secondService.getAllUser(this.idUser)
    .then(response => {
      this.staff = [];
      this.staff = response;
      load.dismiss();
    }).catch(error => {
      console.clear
      load.dismiss();
    })
  }

  //AGREGAR
  create(id:any) {
    let data = {
      app: this.idUser,
      user: id,
      state: 1,
      tipo: 1,
      permiso: 1
    }
    this.secondService.create(data)
    .then(response => {
      this.update(response.id);
    }).catch(error => {
      console.clear
    });
  }

  //ELIMINAR
  delete(id:any) {
    this.secondService.delete(id)
    .then(response => {
      console.log(response)
      if(this.selectItem == 'users') {
        this.getAll();        
      } else {
        this.getAllSecond()
      }
    }).catch(error => {
      console.clear
    });
  }

  //ELIMINAR
  public delete2(id:string, idUser?:any){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar al miembro de Staff?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.secondService.delete(id)
            .then(response => {
              this.getAllSecond()
              this.updateClient(idUser);
              console.clear();
            }).catch(error => {
              console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  public updateClient(idUser:any) {
    let data = {
      inicioMembresia: '',
      finMembresia: '',
      tipoNivel: 0,
      opcion14: 0,
      opcion13: 0,
      opcion15: null,
      id: idUser
    }
    this.mainService.update(data)
    .then(response => {
    }).catch(error => {
      console.log(error);
    })
  }

  //ACTUALIZAR
  update(parameter:any) {
    let chooseModal = this.modal.create('FormStaffUserPage', { parameter });
    chooseModal.onDidDismiss(data => {
      if(data != 'Close') {
        if(this.selectItem == 'users') {
          this.getAll();        
        } else {
          this.getAllSecond()
        }
      } else {
        this.getAll();
      }
    });
    chooseModal.present();
  }

  //ACTUALIZAR
  membership(parameter:any) {
    let chooseModal = this.modal.create('FormStaffMembershipUserPage', { parameter });
    chooseModal.onDidDismiss(data => {
      /*if(data != 'Close') {
        if(this.selectItem == 'users') {
          this.getAll();        
        } else {
          this.getAllSecond()
        }
      } else {
        this.getAll();
      }*/
    });
    chooseModal.present();
  }

  ionViewWillEnter() {
    if(this.selectItem == 'users') {
      this.getAll();        
    } else {
      this.getAllSecond()
    }
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      if(this.selectItem == 'users') {
        this.getAll();        
      } else {
        this.getAllSecond()
      }
      refresher.complete();
    }, 2000);
  }

  //BUSCAR
  public searchTable() {
    var value = this.search.toLowerCase();
    $("#myList ion-item").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

}
