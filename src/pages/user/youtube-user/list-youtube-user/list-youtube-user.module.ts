import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListYoutubeUserPage } from './list-youtube-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    ListYoutubeUserPage,
  ],
  imports: [
    IonicPageModule.forChild(ListYoutubeUserPage),
    PipeModule
  ],
  exports: [
    ListYoutubeUserPage
  ]
})
export class ListYoutubeUserPageModule {}