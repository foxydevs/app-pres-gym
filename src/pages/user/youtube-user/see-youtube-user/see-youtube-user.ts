/**
 * @author    ThemesBuckets <themebucketbd@gmail.com>
 * @copyright Copyright (c) 2018
 * @license   Fulcrumy
 * 
 * This File Represent video Component
 * File path - '../../src/pages/video/video'
 */

import { Component } from '@angular/core';
import { NavController, NavParams, Events, ViewController, IonicPage } from 'ionic-angular';
import { YoutubeProvider } from '../../../../providers/youtube/youtube';
import { StorageProvider } from '../../../../providers/storage/storage';
import { UtilsProvider } from '../../../../providers/utils/utils';

@IonicPage()
@Component({
  selector: 'see-youtube-user',
  templateUrl: 'see-youtube-user.html',
})
export class SeeYoutubeUserPage {
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  // Content of Video
  videoDetails: any;

  // Video Id
  videoId: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    public youtubeProvider: YoutubeProvider,
    public storageProvider: StorageProvider,
    public utilsProvider: UtilsProvider,
    public viewCtrl: ViewController) {

    // Get Video Id
    this.videoId = this.navParams.get('videoId');

    /**
     * Pause Video in Background
     */
    this.events.subscribe('App:Paused', () => {
      this.pauseVideo();
    });

    /**
     * Resume Video
     */
    this.events.subscribe('App:Resume', () => {
      this.playVideo();
    });
  }

  /** Do any initialization */
  ngOnInit() {
    this.getVideoDetails();
  }

  /**
   * --------------------------------------------------------------
   * Get Videos Details Content
   * --------------------------------------------------------------
   */
  getVideoDetails() {
    this.youtubeProvider.getVideoDetails(this.videoId).subscribe(data => {
      this.videoDetails = data.items[0];
    });
  }

  /**
   * --------------------------------------------------------------
   * Convert Text to Html
   * --------------------------------------------------------------
   * Conver Video Description to Html Format
   */
  textToHtml() {
    return this.videoDetails.snippet.description.replace(new RegExp('\n', 'g'), "<br />");
  }

  /**
   * ------------------------------------------------------
   * Play Video
   * ------------------------------------------------------
   */
  playVideo() {
    var iframe = document.getElementsByTagName("iframe")[0].contentWindow;
    iframe.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
  }

  /**
   * ------------------------------------------------------
   * Pause Video
   * ------------------------------------------------------
   */
  pauseVideo() {
    var iframe = document.getElementsByTagName("iframe")[0].contentWindow;
    iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
  }

  /**
   * ------------------------------------------------------
   * Stop Video
   * ------------------------------------------------------
   */
  stopVideo() {
    var iframe = document.getElementsByTagName("iframe")[0].contentWindow;
    iframe.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
  }

  /**
   * ------------------------------------------------------
   * Save Video
   * ------------------------------------------------------
   * This method save video contents in user localstorage as a favorite video.
   * All the saved videos will be shown in favorite tab.
   */
  saveVideo() {
    let storageData = [];
    this.storageProvider.get().then((data: any) => {
      if (data) {
        storageData = data;
        storageData.push(this.videoDetails)
      } else {
        storageData.push(this.videoDetails);
      }
      this.storageProvider.set(storageData);
      this.utilsProvider.presentToast('Video added in favourite', 2000, 'bottom');
    });
  }

  /**
   * Dismiss function
   * This function dismiss the popup modal
   */
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
