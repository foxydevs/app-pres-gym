import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeeYoutubeUserPage } from './see-youtube-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    SeeYoutubeUserPage,
  ],
  imports: [
    IonicPageModule.forChild(SeeYoutubeUserPage),
    PipeModule
  ],
  exports: [
    SeeYoutubeUserPage
  ]
})
export class SeeYoutubeUserPageModule {}