import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormSocialUserPage } from './form-social-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormSocialUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormSocialUserPage),
    PipeModule
  ],
  exports: [
    FormSocialUserPage
  ]
})
export class FormSocialUserPageModule {}