import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SocialUserPage } from './social-user';
 
@NgModule({
  declarations: [
    SocialUserPage,
  ],
  imports: [
    IonicPageModule.forChild(SocialUserPage),
  ],
  exports: [
    SocialUserPage
  ]
})
export class SocialUserPageModule {}