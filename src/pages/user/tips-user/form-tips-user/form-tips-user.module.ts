import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormTipsUserPage } from './form-tips-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormTipsUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormTipsUserPage),
    PipeModule
  ],
  exports: [
    FormTipsUserPage
  ]
})
export class FormTipsUserPageModule {}