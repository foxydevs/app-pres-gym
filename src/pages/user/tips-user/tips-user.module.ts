import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TipsUserPage } from './tips-user';
 
@NgModule({
  declarations: [
    TipsUserPage,
  ],
  imports: [
    IonicPageModule.forChild(TipsUserPage),
  ],
  exports: [
    TipsUserPage
  ]
})
export class TipsUserPageModule {}