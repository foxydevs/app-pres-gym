import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage, ViewController } from 'ionic-angular';
import { UsersService } from '../../../../app/service/users.service';
import { path } from "../../../../app/config.module";
import { Events } from 'ionic-angular/util/events';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ModulesService } from '../../../../app/service/module.service';
import { AccessService } from '../../../../app/service/access.service';
import { ModalController } from '../../../../../node_modules/ionic-angular/components/modal/modal-controller';

//JQUERY
declare var $:any;
declare var google;

@IonicPage()
@Component({
  selector: 'profile-configuration-user',
  templateUrl: 'profile-configuration-user.html'
})
export class ProfileConfigurationUserPage {
  //PROPIEDADES
  parameter:any;
  idUser:any = path.id;
  basePath:string = path.path;
  btnDisabled:boolean;
  btnDisabled2:boolean;
  btnDisabled3:boolean;
  images:boolean;
  design:boolean;
  title:any;
  map: any;
  data:any;
  profile = {
    picture: '',
    username: '',
    email: '',
    firstname: '',
    lastname: '',
    work: '',
    description: '',
    age: '',
    birthday: '',
    phone: '',
    last_latitud: '',
    last_longitud: '',
    youtube_channel: '',
    opcion1: 'Q',
    opcion2: '1',
    opcion3: 0,
    opcion4: 0,
    opcion5: 0,
    opcion6: 0,
    opcion7: 0,
    opcion8: 0,
    opcion16: '',
    opcion17: '',
    pic1: '',
    pic2: '',
    pic3: '',
    color: 'danger',
    color_button: 'danger',
    id: 0
  }
  changePassword = {
    old_pass: '',
	  new_pass : '',
	  new_pass_rep: '',
    id: path.id
  }
  access:any = [];
  table:any = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public mainService: UsersService,
    public secondService: ModulesService,
    public thirdService: AccessService,
    public loading: LoadingController,
    public events: Events,
    public view: ViewController,
    public alertCtrl: AlertController,
    public modalController: ModalController
  ) {
    this.parameter = this.navParams.get('parameter');
    console.log(this.parameter)
    if(this.parameter.state == '1') {
      this.title = 'Actualizar Perfil';
      this.getSingle(this.idUser);
    } else if(this.parameter.state == '2') {
      this.title = 'Cambiar Contraseña';      
    } else if(this.parameter.state == '3') {
      this.title = 'Mi Ubicación'; 
      this.getSingle(this.idUser);
    } else if(this.parameter.state == '4') {
      this.title = 'Control de Accesos'; 
      this.getAllSecond();
    } else if(this.parameter.state == '5') {
      this.title = 'Usuario Encontrado'; 
      this.getSingle(this.parameter.id);
    } else if(this.parameter.state == '6') {
      this.title = 'Otras Configuraciones';
      this.getSingle(this.idUser);
      this.getAllThird();
    } else if(this.parameter.state == '7') {
      this.title = 'Personalización';
      this.getSingle(this.idUser);
    }
  }

  //CARGAR USUARIO
  public getSingle(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(id)
    .then(response => {
      this.profile = response;
      this.data = response;
      this.loadMapUpdate(this.profile.last_latitud, this.profile.last_longitud);
      if(response.opcion3 == true) {
        this.profile.opcion3 = 1;
      } else {
        this.profile.opcion3 = 0;
      }
      load.dismiss();
    }).catch(error => {
      load.dismiss();
      console.log(error)
    })
  }

  //CAMBIAR FOTO DE PERFIL
  uploadImage(archivo, id) {
    let events = this.events;
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}users/upload/${this.idUser}`;
    var size=archivos[0].size;
    var type=archivos[0].type;
    console.log(type)
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
    console.log('DENTRO')

        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
          $("#"+id).upload(url,
          {
            avatar: archivos[0]
          },
          function(respuesta)
          {
            console.log(respuesta)
            $('#imgAvatar').attr("src",respuesta.picture)
            localStorage.setItem('currentPicture', respuesta.picture);
            events.publish('user:updates');
            $("#"+id).val('')
        });
      } else {
        this.message('La imagen es demasiado grande.')
      }
    } else {
      this.message('El tipo de imagen no es válido.');
    }
  }

  //Subir Imagenes de Perfil
  uploadImage1(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}users/upload/pic1/${this.idUser}`;

    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
        if(size<(3*(1024*1024))) {
            $('#imgAvatar1').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
            $("#"+id).upload(url,
                {
                    avatar: archivos[0]
                },
                function(respuesta)
                {
                    $('#imgAvatar1').attr("src",respuesta.pic1)
                    $("#"+id).val('')
                }
            );
        } else {
          this.message('La imagen es demasiado grande.');
        }
    } else {
      this.message('El tipo de imagen no es válido.');
    }
  }

  //Subir Imagenes de Perfil
  uploadImage2(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}users/upload/pic2/${this.idUser}`;

    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
        if(size<(3*(1024*1024))) {
            $('#imgAvatar2').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
            $("#"+id).upload(url,
                {
                    avatar: archivos[0]
                },
                function(respuesta)
                {
                    $('#imgAvatar2').attr("src",respuesta.pic2)
                    $("#"+id).val('')
                }
            );

        } else {
          this.message('La imagen es demasiado grande.');
        }
    } else {
      this.message('El tipo de imagen no es válido.');
    }
  }

  //Subir Imagenes de Perfil
  uploadImage3(archivo, id) {
    let events = this.events;
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}users/upload/pic3/${this.idUser}`;
    console.log(archivos)

    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
        if(size<(3*(1024*1024))) {
            $('#imgAvatar3').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
            $("#"+id).upload(url,
                {
                    avatar: archivos[0]
                },
                function(respuesta)
                {
                  localStorage.setItem('currentPictureCover', respuesta.pic3); 
                  $('#imgAvatar3').attr("src",respuesta.pic3);
                  events.publish('user:updates');
                  $("#"+id).val('')
                }
            );

        } else {
          this.message('La imagen es demasiado grande.');
        }
    } else {
      this.message('El tipo de imagen no es válido.');
    }
  }

  //ACTUALIZAR PERFIL
  public updateProfile(){
    this.profile.picture = $('img[alt="Avatar"]').attr('src');
    this.btnDisabled = true;
    this.mainService.update(this.profile)
    .then(response => {
      localStorage.setItem('currentFirstName', this.profile.firstname);
      localStorage.setItem('currentLastName', this.profile.lastname);
      localStorage.setItem('currentEmail', this.profile.email);
      localStorage.setItem('currentIdYoutube', this.profile.youtube_channel);
      this.events.publish('user:updates');
      this.closeModal();
      this.confirmation('Perfil Actualizado', 'Tu perfil ha sido actualizado exitosamente.');
    }).catch(error => {
      this.btnDisabled = false;
    });
  }

  //GUARDAR CONFIGURACIÓN
  public updateConfiguration() {
    this.btnDisabled3 = true;
    this.mainService.update(this.profile)
    .then(response => {  
      localStorage.setItem('currentColor', response.color);
      localStorage.setItem('currentColorButton', response.color_button);
      localStorage.setItem('currentHome', response.opcion2);
      localStorage.setItem('currentCurrency', response.opcion1);
      localStorage.setItem('currentDesignCategory', response.opcion8);
      if(response.opcion3 == true) {
        localStorage.setItem('currentAppMembresia', '1');
      } else {
        localStorage.setItem('currentAppMembresia', '0');
      }
      this.navColor = response.color;
      this.btnColor = response.color_button;
      this.events.publish('user:updates');
      this.closeModal();
      this.confirmation('Configuración Actualizada', 'La configuración ha sido actualizada exitosamente.')
      this.btnDisabled3 = false;
    }).catch(error => {
      this.btnDisabled3 = false;
    })
  }

  //ACTUALIZAR UBICACION
  public updateLocation(){
    this.btnDisabled = true;
    this.mainService.update(this.profile)
    .then(response => {
      this.closeModal();
      this.confirmation('Ubicación Actualizada', 'Tu ubicación ha sido actualizada exitosamente.');
    }).catch(error => {
      this.btnDisabled = false;
    });
  }

  //CAMBIAR CONTRASEÑA
  changePasswordProfile() {
    if(this.changePassword.old_pass == this.changePassword.new_pass) {
      this.message('No se puede usar la misma contraseña.');
    } else {
      if(this.changePassword.new_pass.length >= 8) {
        if(this.changePassword.new_pass == this.changePassword.new_pass_rep) {
          this.btnDisabled2 = true;
          this.mainService.changePassword(this.changePassword)
          .then(response => {
            this.closeModal();
            this.confirmation('Contraseña Actualizada', 'La contraseña ha sido actualizada exitosamente.');
          }).catch(error => {
            this.btnDisabled2 = false;
            this.message('Contraseña inválida.')
          })
        } else {
          this.message('Las contraseñas no coinciden.');
        }
      } else {
        this.message('La contraseña debe contener al menos 8 caracteres.');        
      }
    }
  }

  //CARGAR MAPA ACTUALIZAR
  public loadMapUpdate(lat:any, lon:any) {
  let latitude = lat;
  let longitude = lon;
  this.profile.last_latitud = latitude.toString();
  this.profile.last_longitud = longitude.toString();
    let mapEle: HTMLElement = document.getElementById('map');
    let myLatLng = new google.maps.LatLng({lat: latitude, lng: longitude});
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });

    var marker;
    marker = new google.maps.Marker({
      map: this.map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: myLatLng
    });

    google.maps.event.addListener(marker, 'dragend', (evt) => {
      this.profile.last_latitud = evt.latLng.lat();
      this.profile.last_longitud = evt.latLng.lng();
    });
  }

  getAllSecond() {
    let load = this.loading.create({
      content: "Cargando..."
    });
    load.present();
    this.secondService.getAll()
    .then(response => {
      this.access.length = [];
      for(let x of response) {
        this.thirdService.getModuleAccess(this.idUser, x.id)
        .then(response1 => {
          if(response1.mostrar == '1') {
            let data = {
              modulo: response1.modulo,
              nombre: x.nombre,
              mostrar: 1, 
              apodo: response1.apodo,
              acceso: response1.id
            }
            this.access.push(data)
          }
          if(response1.mostrar == '0') {
            let data = {
              modulo: response1.modulo,
              nombre: x.nombre,
              mostrar: 0, 
              apodo: response1.apodo,
              acceso: response1.id
            }
            this.access.push(data)
          }
        }).catch(error => {
          if(error.status == '404') {
            console.log('Error de Módulo.');
          }
          let data = {
            modulo: x.id,
            nombre: x.nombre,
            mostrar: 0
          }
          this.access.push(data)
        });
      }
      load.dismiss();
      console.clear
    }).catch(error => {
      if(error.status == '429') {
        load.dismiss();
      }
      console.clear
    });
  }

  //CARGAR MODULOS
  public getAllThird() {
    this.thirdService.getAccess(this.idUser)
    .then(res => {
      this.table = [];
      this.table = res.permitidos;
      console.log(this.table)
    }).catch(error => {
      console.log(error)
    })
  }

  //ACTUALIZAR MODULO
  public saveChanges(a, modulo) {
    let access = {
      mostrar: a,
      modulo: modulo,
      usuario: this.idUser
    }
    this.thirdService.create(access)
    .then(res => {
      if(a == '0') {
        this.message('El módulo ha sido desactivado.');
      } else if(a == '1') {
        this.message('El módulo ha sido activado.');
      }
      this.events.publish('user:updates');
    }).catch(error => {
      console.log(error)
    })
  }

  //CONFIRMACIÓN
  confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 1000
    }).present();
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss('Close');
  }

  //PUSH
  public push() {
    this.navCtrl.push('ProfileConfigurationUserPage')
  }

  showDesign() {
    this.images = false;
    this.design = true;
  }

  showImages() {
    this.images = true;
    this.design = false;
  }

  //ACTUALIZAR MODULO
  public updateModule(parameter:any) {
    let chooseModal = this.modalController.create('ModalModulesUserPage', { parameter });
    chooseModal.onDidDismiss((data) => {
      if(data!='Close') {
        this.getAllSecond();
      }
    });
    chooseModal.present();
  }

}
