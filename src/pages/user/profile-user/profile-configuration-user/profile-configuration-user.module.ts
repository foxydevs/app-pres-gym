import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfileConfigurationUserPage } from './profile-configuration-user';
 
@NgModule({
  declarations: [
    ProfileConfigurationUserPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfileConfigurationUserPage),
  ],
  exports: [
    ProfileConfigurationUserPage
  ]
})
export class ProfileConfigurationUserPageModule {}