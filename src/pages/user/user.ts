import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, IonicPage } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { UsersService } from '../../app/service/users.service';
import { path } from "./../../app/config.module";
import { Events } from 'ionic-angular/util/events';
import { AccessService } from '../../app/service/access.service';
import { NavController } from '../../../node_modules/ionic-angular/navigation/nav-controller';

@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html'
})
export class UserPage {
  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  pages: Array<{icon:string, ios:string, title: string, apodo?: string, component: any}>;
  picture:any;
  email:any;
  firstName:any;
  baseUser:any;
  baseId:number = path.id;
  pictureCover:any;
  appMembresia:any;
  authentication = localStorage.getItem('currentAuthentication');

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public loading: LoadingController,
    public events: Events,
    public usersService: UsersService,
    public accessService: AccessService,
    public navCtrl: NavController) {
    this.picture = localStorage.getItem("currentPicture");
    this.email = localStorage.getItem("currentEmail");
    this.firstName = localStorage.getItem("currentFirstName") + ' ' + localStorage.getItem("currentLastName");
    this.appMembresia = localStorage.getItem('currentAppMembresia');
    // console.log(this.baseId)
    this.loadSingleUser();
    // used for an example of ngFor and navigation
    events.subscribe('user:updates', res => {
      this.authentication = localStorage.getItem('currentAuthentication');
      this.appMembresia = localStorage.getItem('currentAppMembresia');
      this.pictureCover = localStorage.getItem('currentPictureCover');
      this.picture = localStorage.getItem("currentPicture");
      this.email = localStorage.getItem("currentEmail");
      this.firstName = localStorage.getItem("currentFirstName") + ' ' + localStorage.getItem("currentLastName");
      this.loadModules();
      if(this.baseId == 82) {
        this.rootPage = 'CategorysDocumentsUserPage';
      } else {
        this.rootPage = 'CategorysUserPage';
      }
    });
    this.loadModules();
    if(this.baseId == 82) {
      this.rootPage = 'CategorysDocumentsUserPage';
    } else {
      this.rootPage = 'CategorysUserPage';
    }
  }

  public loadModules() {
    this.accessService.getAccess(this.baseId)
    .then(response => {      
      if(response.permitidos.length > 0){
        this.pages = [];
        for(let x of response.permitidos) {
          if(x.modulos_show.id == '16' && x.mostrar == '1') {
            if(this.appMembresia == '1') {
              this.pages.push({icon: 'md-star', ios: 'ios-star', title: x.modulos_show.nombre, apodo: x.apodo, component: 'MembershipUserPage'})
            } 
          } else if(x.modulos_show.id == '1' && x.mostrar == '1') {
            if(this.baseId == 82) {
              this.pages.push({icon: 'md-apps', ios: 'ios-apps', title: x.modulos_show.nombre, apodo: x.apodo, component: 'CategorysDocumentsUserPage'})            
            } else {
              this.pages.push({icon: 'md-apps', ios: 'ios-apps', title: x.modulos_show.nombre, apodo: x.apodo, component: 'CategorysUserPage'})
            }
          } else if(x.modulos_show.id == '2' && x.mostrar == '1') {
            if(this.baseId == 82) {
              this.pages.push({icon: 'md-document', ios: 'ios-document', title: x.modulos_show.nombre, apodo: x.apodo, component: 'DocumentsUserPage'})
            } else {
              this.pages.push({icon: 'md-albums', ios: 'ios-albums', title: x.modulos_show.nombre, apodo: x.apodo, component: 'ProductsUserPage'})              
            }
          } else if(x.modulos_show.id == '3' && x.mostrar == '1') {
            this.pages.push({icon: 'md-cart', ios: 'ios-cart', title: x.modulos_show.nombre, apodo: x.apodo, component: 'OrdersUserPage'})
          } else if(x.modulos_show.id == '4' && x.mostrar == '1') {
            this.pages.push({icon: 'md-calendar', ios: 'ios-calendar', title: x.modulos_show.nombre, apodo: x.apodo, component: 'EventsUserPage'})
          } else if(x.modulos_show.id == '5' && x.mostrar == '1') {
            this.pages.push({icon: 'logo-youtube', ios: 'logo-youtube', title: x.modulos_show.nombre, apodo: x.apodo, component: 'YoutubeUserPage'})
          } else if(x.modulos_show.id == '6' && x.mostrar == '1') {
            this.pages.push({icon: 'md-globe', ios: 'ios-globe', title: x.modulos_show.nombre, apodo: x.apodo, component: 'NewsUserPage'})
          } else if(x.modulos_show.id == '7' && x.mostrar == '1') {
            if(this.baseId == 49) {
              this.pages.push({icon: 'md-nutrition', ios: 'ios-nutrition', title: x.modulos_show.nombre, apodo: x.apodo, component: 'TipsUserPage'})
            } else {
              this.pages.push({icon: 'md-alarm', ios: 'ios-alarm', title: x.modulos_show.nombre, apodo: x.apodo, component: 'TipsUserPage'})
            }
          } else if(x.modulos_show.id == '8' && x.mostrar == '1') {
            this.pages.push({icon: 'md-send', ios: 'ios-send', title: x.modulos_show.nombre, apodo: x.apodo, component: 'MessagesTabPage'})
          } else if(x.modulos_show.id == '9' && x.mostrar == '1') {
            this.pages.push({icon: 'md-person', ios: 'ios-person', title: x.modulos_show.nombre, apodo: x.apodo, component: 'ProfileUserPage'})
          } else if(x.modulos_show.id == '10' && x.mostrar == '1') {
            this.pages.push({icon: 'md-walk', ios: 'ios-walk', title: x.modulos_show.nombre, apodo: x.apodo, component: 'WorkoutsUserPage'})
          } else if(x.modulos_show.id == '11' && x.mostrar == '1') {
            this.pages.push({icon: 'md-medal', ios: 'ios-medal', title: x.modulos_show.nombre, apodo: x.apodo, component: 'LeadearboardUserPage'})
          } else if(x.modulos_show.id == '12' && x.mostrar == '1') {
            this.pages.push({icon: 'logo-rss', ios: 'logo-rss', title: x.modulos_show.nombre, apodo: x.apodo, component: 'BlogUserPage'})
          } else if(x.modulos_show.id == '13' && x.mostrar == '1') {
            this.pages.push({icon: 'md-thumbs-up', ios: 'ios-thumbs-up', title: x.modulos_show.nombre, apodo: x.apodo, component: 'SocialUserPage'})
          } else if(x.modulos_show.id == '14' && x.mostrar == '1') {
            this.pages.push({icon: 'md-play', ios: 'ios-play', title: x.modulos_show.nombre, apodo: x.apodo, component: 'ActivePauseUserPage'})
          } else if(x.modulos_show.id == '15' && x.mostrar == '1') {
            this.pages.push({icon: 'md-pricetag', ios: 'ios-pricetag', title: x.modulos_show.nombre, apodo: x.apodo, component: 'PromotionUserPage'})
          } else if(x.modulos_show.id == '17' && x.mostrar == '1') {
            this.pages.push({icon: 'md-pricetags', ios: 'ios-pricetags', title: x.modulos_show.nombre, apodo: x.apodo, component: 'BenefitsUserPage'})
          } else if(x.modulos_show.id == '18' && x.mostrar == '1') {
            this.pages.push({icon: 'md-body', ios: 'ios-body', title: x.modulos_show.nombre, apodo: x.apodo, component: 'ProgressUserPage'})
          } else if(x.modulos_show.id == '19' && x.mostrar == '1') {
            this.pages.push({icon: 'md-stopwatch', ios: 'ios-stopwatch', title: x.modulos_show.nombre, apodo: x.apodo, component: 'RunningClubUserPage'})
          } else if(x.modulos_show.id == '20' && x.mostrar == '1') {
            this.pages.push({icon: 'md-people', ios: 'ios-people', title: x.modulos_show.nombre, apodo: x.apodo, component: 'ZFitClubUserPage'})
          } else if(x.modulos_show.id == '21' && x.mostrar == '1') {
            this.pages.push({icon: 'md-planet', ios: 'ios-planet', title: x.modulos_show.nombre, apodo: x.apodo, component: 'StaffUserPage'})
          } else if(x.modulos_show.id == '22' && x.mostrar == '1') {
            this.pages.push({icon: 'md-leaf', ios: 'ios-leaf', title: x.modulos_show.nombre, apodo: x.apodo, component: 'MotivationsUserPage'})
          } else if(x.modulos_show.id == '23' && x.mostrar == '1') {
            this.pages.push({icon: 'md-alarm', ios: 'ios-alarm', title: x.modulos_show.nombre, apodo: x.apodo, component: 'CitesUserPage'})
          } else if(x.modulos_show.id == '24' && x.mostrar == '1') {
            this.pages.push({icon: 'md-information-circle', ios: 'ios-information-circle', title: x.modulos_show.nombre, apodo: x.apodo, component: 'InformationUserPage'})
          }
        }
      }
      console.clear
    }).catch(error => {
      console.log(error);
    })
  }

  public loadSingleUser() {
    if((this.baseId+'')!='null'){
      this.usersService.getSingle(this.baseId)
      .then(response => {
        this.baseUser = response;
        this.pictureCover = response.pic3;
        if(response.opcion3 == true) {
          localStorage.setItem('currentAppMembresia', '1');
        } else {
          localStorage.setItem('currentAppMembresia', '0');
        }
        localStorage.setItem('currentPictureCover', response.pic3);
      }).catch(error => {
        console.clear();
      })
    }
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  openLeadearboard() {
    this.nav.setRoot('LeadearboardUserPage');
  }

  openBlog() {
    this.nav.setRoot('BlogUserPage');
  }

  openWorkouts() {
    this.nav.setRoot('WorkoutsMonthUserPage');
  }

  openPersonalization() {
    this.nav.setRoot('PersonalizationUserPage');
  }

  openAdoptions() {
    this.nav.setRoot('AdoptionUserPage');
  }

  openSocial() {
    this.nav.setRoot('SocialUserPage');
  }

  openActivePause() {
    this.nav.setRoot('ActivePauseUserPage');
  }

  openRunningClub() {
    this.nav.setRoot('RunningClubUserPage');
  }

  openBenefits() {
    this.nav.setRoot('BenefitsUserPage');
  }

  openProgress() {
    this.nav.setRoot('ProgressUserPage');
  }

  openZFit() {
    this.nav.setRoot('ZFitClubUserPage');
  }

  openStaff() {
    this.nav.setRoot('StaffUserPage');
  }

  openMotivation() {
    this.nav.setRoot('MotivationsUserPage');
  }

  openDemo() {
    this.nav.setRoot('DemoMapsPage');
  }

  logOut() {
    localStorage.clear();
    this.loading.create({
      content: "Cerrando Sesión...",
      duration: 1000
      }).present();
    this.events.publish('user:update');
    this.rootPage = 'ClientPage';
    localStorage.setItem('currentAuthentication', 'NoAuthentication')
  }
}
