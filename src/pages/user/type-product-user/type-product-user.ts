import { Component } from '@angular/core';
import { NavController, IonicPage} from 'ionic-angular';
import { LoadingController } from '../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { PresentationService } from '../../../app/service/presentation.service';
import { path } from '../../../app/config.module';

@IonicPage()
@Component({
  selector: 'type-product-user',
  templateUrl: 'type-product-user.html',
})
export class TypeProductUserPage {
  //PROPIEDADES
  public Table:any[] = [];
  public idUser:any = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  selectItem:any = 'beneficios';

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public mainService: PresentationService,
  ) {
  }

  openForm(parameter?:any) {
    this.navCtrl.push('FormTypeProductUserPage', { parameter });
  }

  //CARGAR LOS RETOS
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAll()
    .then(response => {
      this.Table = []
      this.Table = response;
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  ionViewWillEnter() {
    this.getAll();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll();
      refresher.complete();
    }, 2000);
  }

}
