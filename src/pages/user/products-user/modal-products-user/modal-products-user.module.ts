import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PipeModule } from '../../../../pipes/pipes.module';
import { ModalProductsUserPage } from './modal-products-user';
import { PresentationService } from '../../../../app/service/presentation.service';
 
@NgModule({
  declarations: [
    ModalProductsUserPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalProductsUserPage),
    PipeModule
  ],
  exports: [
    ModalProductsUserPage
  ],
  providers: [
    PresentationService
  ]
})
export class ModalProductsUserPageModule {}