import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, IonicPage, ModalController } from 'ionic-angular';
import { ProductsService } from '../../../app/service/products.service';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'products-user',
  templateUrl: 'products-user.html'
})
export class ProductsUserPage {
  //PROPIEDADES
  products:any[] = [];
  parameter:any;
  idUser:any;
  search:any;
  appMembresia = localStorage.getItem('currentAppMembresia');
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public productsService: ProductsService,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public navParams: NavParams,
    public modalController: ModalController
  ) {
    this.parameter = this.navParams.get('parameter');
    this.idUser = localStorage.getItem("currentId");
    if(this.parameter) {
      this.getAllByCategories(this.parameter);
    } else {
      this.getAll();
    }
  }

  //CARGAR PRODUCTOS POR CATEGORIA
  public getAllByCategories(id:any){
    this.productsService.getAllUser(this.idUser)
    .then(response => {
      this.products = [];
      for(let x of response) {
        if(x.category == id) {
          this.products.push(x);
        }
      }
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR PRODUCTOS
  public getAll() {
    this.productsService.getAllUser(this.idUser)
    .then(response => {
      this.products = [];
      this.products = response;
    }).catch(error => {
      console.clear
    })
  }

  public seeOrders(parameter:any) {
    this.loading.create({
      content: "Cargando",
      duration: 750
    }).present();
    this.navCtrl.push('OrdersUserPage', { parameter });
  }

  public seeMore(parameter:any) {
    this.navCtrl.push('SeeProductsUserPage', { parameter });
  }

  //Ver Formulario Agregar
  public viewForm() {
    var parameter = this.parameter;
    if(this.parameter) {
      this.navCtrl.push('ProductsFormUserPage', { parameter });
    } else {
      this.navCtrl.push('ProductsFormUserPage');
    }
  }

  public viewUpdateForm(parameter:any) {
    this.navCtrl.push('ProductsFormUpdateUserPage', { parameter });
  }

  ionViewWillEnter() {
    if(this.parameter) {
      this.getAllByCategories(this.parameter);
    } else {
      this.getAll();
    }
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar el producto?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando...",
              duration: 2000
            })
            load.present()
            this.productsService.delete(id)
            .then(res => {
              load.dismiss();
              this.getAll();
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //BUSCAR
  public searchTable() {
    var value = this.search.toLowerCase();
    $("#myList ion-card").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      if(this.parameter) {
        this.getAllByCategories(this.parameter);
      } else {
        this.getAll();
      }
      refresher.complete();
    }, 2000);
  }

  openTypes() {
    this.navCtrl.push('TypeProductUserPage')
  }

  //OPEN MODAL PRODUCTS
  public openModal(state:any, category?:any, id?:'') {
    let parameter = {
      state: state,
      category: category,
      id: id
    }
    let chooseModal = this.modalController.create('ModalProductsUserPage', { parameter });
    chooseModal.onDidDismiss((data) => {
      if(this.parameter) {
        this.getAllByCategories(this.parameter);
      } else {
        this.getAll();
      }     
    });
    chooseModal.present();
  }

  //SUMAR ORDEN
  addOrder(p:any) {
    for (var x in this.products) {
      if (this.products[x] == p) {
        this.products[x].order = +this.products[x].order + 1;
        this.update(this.products[x]);
        console.log(this.products[x].order)
      }
    }
  }

  //SUMAR ORDEN
  removeOrder(p:any) {
    for (var x in this.products) {
      if (this.products[x] == p) {
        if(this.products[x].order > 1) {
          this.products[x].order = +this.products[x].order - 1;
          this.update(this.products[x]);
          console.log(this.products[x].order)
        }
      }
    }
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.productsService.update(formValue)
    .then(response => {
      this.getAllByCategories(this.parameter);
    }).catch(error => {
      console.clear
    });
  }

}
