import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, IonicPage } from 'ionic-angular';
import { UsersService } from '../../../../app/service/users.service';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'messages-contacts-user',
  templateUrl: 'messages-contacts-user.html'
})
export class MessagesContactsUserPage {
  //PROPIEDADES
  private idUser:any;
  private users:any[] = [];
  private usersSend:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  public search:any;
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loading: LoadingController,
    public usersService: UsersService
  ) {
    this.idUser = localStorage.getItem("currentId");
    this.loadAllUsers();
  }

  //CARGAR USUARIOS
  public loadAllUsers(){
    this.usersService.getClients(this.idUser)
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear
    })
  }

  public sendMessage(clients:any[]) {
    let parameter = {
      parameter: clients,
      message: 'New'
    }
    console.log(parameter)
    this.navCtrl.push('SendMessagesClientUserPage', { parameter });
  }

  ionViewWillEnter() {
    this.loadAllUsers();
  }

  //BUSCAR
  public searchTable() {
    var value = this.search.toLowerCase();
    $("#myList ion-item").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

  //MANDAR MSJ
  sendMessages(state:any, e:any) {
    e.dueno = state;
    this.usersSend.push(e);
    console.log('UPDATE', this.usersSend)
  }

  //MANDAR MSJ
  deleteMessages(state:any, e:any) {
    e.dueno = state;
    this.usersSend.splice(this.usersSend.indexOf(e),1)
    console.log('UPDATE', this.usersSend)
  }

}
