import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessagesTabPage } from './messages-tab';
 
@NgModule({
  declarations: [
    MessagesTabPage,
  ],
  imports: [
    IonicPageModule.forChild(MessagesTabPage),
  ],
  exports: [
    MessagesTabPage
  ]
})
export class MessagesTabPageModule {}