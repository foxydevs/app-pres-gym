import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, Content, IonicPage } from 'ionic-angular';
import { MessagesService } from '../../../../app/service/messages.service';
import { Platform } from 'ionic-angular/platform/platform';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'messages-client-user',
  templateUrl: 'messages-client-user.html'
})
export class MessagesClientUserPage {
  @ViewChild(Content) content: Content;

  //PROPIEDADES 
  private messages:any[] = [];
  private message = {
    subject: 'Sin Asunto',
    message : '',
    user_send: '',
    user_receipt: '',
    picture: '',
    tipo: 0,
  }
  private parameter:any;
  private idUserSend:any;
  private idUserReceipt:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loading: LoadingController,
    public messagesService: MessagesService,
    private platform: Platform,
    private iab: InAppBrowser
  ) {
    this.parameter = this.navParams.get('parameter');
    this.idUserSend = localStorage.getItem('currentId');
    this.idUserReceipt = this.parameter;    
    this.message.user_send = localStorage.getItem('currentId');
    this.message.user_receipt = this.parameter;
  }

  public loadMessages(user_receipt:any) {
    var idUser = localStorage.getItem("currentId");
    this.messagesService.getAll()
    .then(response => {  
      this.messages = []    
      for(let x of response) {
        if((x.user_receipt == user_receipt && x.user_send == idUser) 
        || x.user_send == user_receipt && x.user_receipt == idUser) {
          this.messages.push(x);       
        }            
      }
      this.messages.reverse();
    }).catch(error => {
      console.clear();
    })
  }

  public sendMessage(parameter:any) {
    this.navCtrl.push('SendMessagesClientUserPage', { parameter })
  }

  ionViewWillEnter() {
    this.loadMessages(this.parameter);
  }

  //AGREGAR
  sendMessages() {
    console.log(this.message)
    this.messagesService.create(this.message)
    .then(response => {
      console.log(response);
      this.message.message = '';
      this.loadMessages(this.parameter);
    }).catch(error => {
      console.clear
    });
  }

  //ACTUALIZAR MENSAJES
  doRefresh(refresher) {
    setTimeout(() => {
      this.loadMessages(this.parameter);
      refresher.complete();
    }, 2000);
  }

  //CARGAR PAGINA
  openDocument(urlObject:any, title:any) {
    if (this.platform.is('android')) {
      this.iab.create('http://docs.google.com/gview?url=' + urlObject, '_blank', 'presentationstyle=pagesheet,toolbarcolor=#003D6E');      
    } else {
      this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet');
    }
  }

  //CARGAR PAGINA
  openBrowser(urlObject:any) {
    this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet,toolbarcolor=#E67E22');
  }
}