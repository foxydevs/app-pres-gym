import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage } from 'ionic-angular';
import { MessagesService } from '../../../../../app/service/messages.service';
import { path } from '../../../../../app/config.module';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'messages-client-send-user',
  templateUrl: 'messages-client-send-user.html'
})
export class SendMessagesClientUserPage{
  private message = {
    subject: '',
    message : '',
    user_send: '',
    user_receipt: '',
    link1: '',
    link2: '',
    tipo: 0,
    picture: 'https://images.vexels.com/media/users/3/136394/isolated/preview/83b45fcfc188a8f4a9daef936855b019-icono-de-mensaje-by-vexels.png',
  }
  baseId:number = path.id;
  private parameter:any;
  private users:any[];
  private basePath:string = path.path;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public messagesService: MessagesService,
    public loading: LoadingController
  ) {
    this.parameter = this.navParams.get('parameter');
    console.log(this.parameter)
    if(this.parameter.message == 'New') {
      //this.message.user_receipt = 
      this.users = this.parameter.parameter;
    } else {
      this.message.user_receipt = this.parameter;
    }
    this.message.user_send = localStorage.getItem('currentId');
  }

   //ENVIAR MENSAJE
   public sendMessage() {
    this.message.picture = $('img[alt="Avatar"]').attr('src');
    if(this.message.subject) {
      if(this.message.message) {
        if(this.message.picture == 'https://images.vexels.com/media/users/3/136394/isolated/preview/83b45fcfc188a8f4a9daef936855b019-icono-de-mensaje-by-vexels.png') {
          this.message.picture = ''
          this.messagesService.create(this.message)
          .then(response => {
            let loader = this.loading.create({
              content: "Enviando Mensaje...",
              duration: 2000
            });
            loader.present();
            this.returnMessages(this.message.user_receipt)
            console.clear
          }).catch(error => {
            console.clear
          })
        } else {
          this.messagesService.create(this.message)
          .then(response => {
            let loader = this.loading.create({
              content: "Enviando Mensaje...",
              duration: 2000
            });
            loader.present();
            this.returnMessages(this.message.user_receipt)
            console.clear
          }).catch(error => {
            console.clear
          })
        }        
      } else {
        this.messages('El mensaje es requerido.');
      }
    } else {
      this.messages('El asunto es requerido.')
    }    
  }

  //IMAGEN DE CATEGORIA
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    console.log(archivos[0])
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'messages'
          },
          function(respuesta) {
            $('#imgAvatar').attr("src", respuesta.url)
            $("#"+id).val('')
          }
        );
      } else {
        this.messages('La imagen es demasiado grande.')
      }
    } else {
      this.messages('El tipo de imagen no es válido.')
    }
  }

  //REGRESAR A MENSAJES ANTERIORES
  public returnMessages(parameter:any) {
    if(this.parameter.message == 'New') {
      this.navCtrl.pop();
      this.navCtrl.push('MessagesClientUserPage', { parameter });
    } else {
      this.navCtrl.pop();
    }
  }

  //MENSAJES
  public messages(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

  //SEND MASIVE
  public sendMasive() {
    this.message.picture = $('img[alt="Avatar"]').attr('src');
    if(this.message.subject) {
      if(this.message.message) {
        if(this.message.picture == 'https://images.vexels.com/media/users/3/136394/isolated/preview/83b45fcfc188a8f4a9daef936855b019-icono-de-mensaje-by-vexels.png') {
          this.message.picture = ''
          let loader = this.loading.create({
            content: "Enviando Mensaje..." 
          });
          loader.present();
          this.users.forEach(element => {
            let data = {
              subject: this.message.subject,
              message : this.message.message,
              user_send: this.message.user_send,
              user_receipt: element.id,
              picture: this.message.picture,
              link1: this.message.link1,
              link2: this.message.link2,
              tipo: 0,
            }
            this.messagesService.create(data)
            .then(response => {
            console.clear
            }).catch(error => {
              console.clear
            });
          });
          loader.dismiss();
          this.navCtrl.pop();
        }
      } else {
        this.messages('El mensaje es requerido.');
      }
    } else {
      this.messages('El asunto es requerido.')
    }   
  }

}