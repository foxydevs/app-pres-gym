import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessagesClientUserPage } from './messages-client-user';
 
@NgModule({
  declarations: [
    MessagesClientUserPage,
  ],
  imports: [
    IonicPageModule.forChild(MessagesClientUserPage),
  ],
  exports: [
    MessagesClientUserPage
  ]
})
export class MessagesClientUserPageModule {}