import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActivePauseUserPage } from './active-pause-user';
 
@NgModule({
  declarations: [
    ActivePauseUserPage,
  ],
  imports: [
    IonicPageModule.forChild(ActivePauseUserPage),
  ],
  exports: [
    ActivePauseUserPage
  ]
})
export class ActivePauseUserPageModule {}