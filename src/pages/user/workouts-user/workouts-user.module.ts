import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WorkoutsUserPage } from './workouts-user';
 
@NgModule({
  declarations: [
    WorkoutsUserPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkoutsUserPage),
  ],
  exports: [
    WorkoutsUserPage
  ]
})
export class WorkoutsUserPageModule {}