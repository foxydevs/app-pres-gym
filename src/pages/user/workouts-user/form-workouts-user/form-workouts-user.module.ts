import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormWorkoutsUserPage } from './form-workouts-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormWorkoutsUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormWorkoutsUserPage),
    PipeModule
  ],
  exports: [
    FormWorkoutsUserPage
  ]
})
export class FormWorkoutsUserPageModule {}