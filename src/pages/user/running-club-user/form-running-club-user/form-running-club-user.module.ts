import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormRunningClubUserPage } from './form-running-club-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormRunningClubUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormRunningClubUserPage),
    PipeModule
  ],
  exports: [
    FormRunningClubUserPage
  ]
})
export class FormRunningClubUserPageModule {}