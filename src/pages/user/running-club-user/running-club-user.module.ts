import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RunningClubUserPage } from './running-club-user';
 
@NgModule({
  declarations: [
    RunningClubUserPage,
  ],
  imports: [
    IonicPageModule.forChild(RunningClubUserPage),
  ],
  exports: [
    RunningClubUserPage
  ]
})
export class RunningClubUserPageModule {}