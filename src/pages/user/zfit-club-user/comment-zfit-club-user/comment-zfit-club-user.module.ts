import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommentZFitClubUserPage } from './comment-zfit-club-user';
 
@NgModule({
  declarations: [
    CommentZFitClubUserPage,
  ],
  imports: [
    IonicPageModule.forChild(CommentZFitClubUserPage),
  ],
  exports: [
    CommentZFitClubUserPage
  ]
})
export class CommentZFitClubUserPageModule {}