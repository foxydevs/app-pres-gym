import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, IonicPage } from 'ionic-angular';
import { LoadingController } from '../../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { PeriodosService } from '../../../../app/service/periodos.service';

@IonicPage()
@Component({
  selector: 'workouts-day-user',
  templateUrl: 'workouts-day-user.html'
})
export class WorkoutsDayUserPage {
  //PROPIEDADES
  table:any[] = [];
  parameter:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public mainService: PeriodosService,
    public navParams: NavParams,
    public modal: ModalController,
  ) {
  }

  //OBTENER DATOS
  getAll(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.table = []
      this.table = response.childs;
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //VER DETALLES DE LA ORDEN
  openModal(id:any) {
    let parameter = {
      parent: this.parameter,
      id: id
    }
    console.log(parameter)
    let chooseModal = this.modal.create('FormWorkoutsDayUserPage', { parameter });
    chooseModal.onDidDismiss(data => {
      this.getAll(this.parameter);  
    });
    chooseModal.present();
  }

  openForm(parameter:any) {
    this.navCtrl.push('RutinasUserPage', { parameter })
  }

  ionViewWillEnter() {
    this.parameter = this.navParams.get('parameter');
    this.getAll(this.parameter);
  }
}