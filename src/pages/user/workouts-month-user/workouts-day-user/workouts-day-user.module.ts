import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WorkoutsDayUserPage } from './workouts-day-user';
 
@NgModule({
  declarations: [
    WorkoutsDayUserPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkoutsDayUserPage),
  ],
  exports: [
    WorkoutsDayUserPage
  ]
})
export class WorkoutsDayUserPageModule {}