import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { ModalController } from '../../../../node_modules/ionic-angular/components/modal/modal-controller';
import { LoadingController } from '../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { PeriodosService } from '../../../app/service/periodos.service';
import { path } from '../../../app/config.module';

@IonicPage()
@Component({
  selector: 'workouts-month-user',
  templateUrl: 'workouts-month-user.html'
})
export class WorkoutsMonthUserPage {
  //PROPIEDADES
  table:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  constructor(
    public navCtrl: NavController,
    public mainService: PeriodosService,
    public loading: LoadingController,
    public modal: ModalController
  ) {
  }

  //CARGAR LOS RETOS
  public getAll(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllUser(id)
    .then(response => {
      this.table = []
      this.table = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  openForm(parameter:any) {
    this.navCtrl.push('WorkoutsDayUserPage', { parameter })
  }

  //VER DETALLES DE LA ORDEN
  openModal(parameter:any) {
    console.log(parameter)
    let chooseModal = this.modal.create('FormWorkoutsMonthUserPage', { parameter });
    chooseModal.onDidDismiss(data => {
      this.getAll(path.id);
    });
    chooseModal.present();
  }

  ionViewWillEnter() {
    this.getAll(path.id);
  }
}