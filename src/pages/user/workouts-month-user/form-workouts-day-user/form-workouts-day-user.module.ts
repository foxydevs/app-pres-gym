import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormWorkoutsDayUserPage } from './form-workouts-day-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormWorkoutsDayUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormWorkoutsDayUserPage),
    PipeModule
  ],
  exports: [
    FormWorkoutsDayUserPage
  ]
})
export class FormWorkoutsDayUserPageModule {}