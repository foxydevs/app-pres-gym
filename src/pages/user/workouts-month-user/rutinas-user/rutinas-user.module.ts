import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RutinasUserPage } from './rutinas-user';
 
@NgModule({
  declarations: [
    RutinasUserPage,
  ],
  imports: [
    IonicPageModule.forChild(RutinasUserPage),
  ],
  exports: [
    RutinasUserPage
  ]
})
export class RutinasUserPageModule {}