import { Component } from '@angular/core';
import { NavController, IonicPage} from 'ionic-angular';
import { LoadingController } from '../../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { ModalController } from '../../../../../node_modules/ionic-angular/components/modal/modal-controller';
import { NavParams } from '../../../../../node_modules/ionic-angular/navigation/nav-params';
import { RutinasService } from '../../../../app/service/rutinas.service';

@IonicPage()
@Component({
  selector: 'rutinas-user',
  templateUrl: 'rutinas-user.html',
})
export class RutinasUserPage {
  //PROPIEDADES
  private table:any[] = [];
  private idUser:any;
  private parameter:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public modal: ModalController,
    public mainService: RutinasService,
    public navParams: NavParams
  ) {
    this.idUser = localStorage.getItem('currentId');
    this.parameter = this.navParams.get('parameter');
  }

  openForm(parameter?:any) {
    this.navCtrl.push('FormRutinaUserPage', { parameter });
  }

  //VER DETALLES DE LA ORDEN
  openModal(parameter:any) {
    console.log(parameter)
    let chooseModal = this.modal.create('FormRutinaUserPage', { parameter });
    chooseModal.onDidDismiss(data => {
      if(data != 'Close') {
        this.getAll(this.idUser);
      }      
    });
    chooseModal.present();
  }


  //CARGAR LOS RETOS
  public getAll(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllUser(id)
    .then(response => {
      this.table = []
      this.table = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  ionViewWillEnter() {
    this.getAll(this.idUser);
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll(this.idUser);
      refresher.complete();
    }, 2000);
  }

}
