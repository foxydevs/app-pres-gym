import { Component } from '@angular/core';
import { NavController, LoadingController, ViewController, IonicPage} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { path } from '../../../../app/config.module';
import { UsersService } from '../../../../app/service/users.service';
import { PeriodosService } from '../../../../app/service/periodos.service';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'form-workouts-month-user',
  templateUrl: 'form-workouts-month-user.html',
})
export class FormWorkoutsMonthUserPage {
  //PROPIEDADES
  public parameter:any;
  public title:any;
  public disabledBtn:boolean;
  public basePath:string = path.path;
  public data = {
    title: '',
    description: '',
    video: '',
    picture: localStorage.getItem('currentPicture'),
    app: path.id,
    type: 0,
    state: 0,
    user_created: path.id,
    id: ''
  }
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    public toast: ToastController,
    public mainService: PeriodosService,
    public secondService: UsersService,
    public view: ViewController
  ) {
    this.parameter = this.navParams.get('parameter');
    console.log(this.parameter)
    if(this.parameter) {
      this.title = "Edición Periodo Semana";
      this.getSingle(this.parameter);  
    } else {
      this.title = "Agregar Periodo Semana";
    }
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss('Close');
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    this.data.picture = $('img[alt="Avatar"]').attr('src');
    if(this.data.title) {
      if(this.data.description) {
        this.disabledBtn = true;
        if(this.parameter) {
          console.log(this.data)
          this.update(this.data)
        } else {
          console.log(this.data)
          this.create(this.data)
        }
      } else {
        this.message('La descripción es requerida.');
      }
    } else {
      this.message('El título es requerido.');
    }
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .then(response => {
      this.confirmation('Periodo Agregado', 'El periodo fue agregado exitosamente.');
      this.parameter = response.id;
      this.title = 'Edición Periodo Semana';
      this.data.id = response.id;
      this.disabledBtn = false;
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .then(response => {
      this.confirmation('Periodo Actualizado', 'El periodo fue actualizado exitosamente.');
      this.navCtrl.pop();
      console.log(response);
      this.disabledBtn = false;
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar el workout?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando..."
            });
            load.present();
            this.mainService.delete(id)
            .then(response => {
              load.dismiss();
              this.navCtrl.pop();
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //IMAGEN
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    console.log(archivos[0])
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'workouts'
          },
          function(respuesta) {
            $('#imgAvatar').attr("src", respuesta.url)
            $("#"+id).val('')
          }
        );
      } else {
        this.message('La imagen es demasiado grande.')
      }
    } else {
      this.message('El tipo de imagen no es válido.')
    }
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

}
