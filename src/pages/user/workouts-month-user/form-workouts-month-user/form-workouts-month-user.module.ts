import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormWorkoutsMonthUserPage } from './form-workouts-month-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormWorkoutsMonthUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormWorkoutsMonthUserPage),
    PipeModule
  ],
  exports: [
    FormWorkoutsMonthUserPage
  ]
})
export class FormWorkoutsMonthUserPageModule {}