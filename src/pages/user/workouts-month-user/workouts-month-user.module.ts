import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WorkoutsMonthUserPage } from './workouts-month-user';
 
@NgModule({
  declarations: [
    WorkoutsMonthUserPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkoutsMonthUserPage),
  ],
  exports: [
    WorkoutsMonthUserPage
  ]
})
export class WorkoutsMonthUserPageModule {}