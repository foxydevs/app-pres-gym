import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormRutinaUserPage } from './form-rutina-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormRutinaUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormRutinaUserPage),
    PipeModule
  ],
  exports: [
    FormRutinaUserPage
  ]
})
export class FormRutinaUserPageModule {}