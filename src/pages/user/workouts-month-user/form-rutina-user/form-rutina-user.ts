import { Component } from '@angular/core';
import { NavController, LoadingController, ViewController, IonicPage} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { path } from '../../../../app/config.module';
import { UsersService } from '../../../../app/service/users.service';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { WorkoutsService } from '../../../../app/service/workouts.service';
import { RutinasService } from '../../../../app/service/rutinas.service';

@IonicPage()
@Component({
  selector: 'form-rutina-user',
  templateUrl: 'form-rutina-user.html',
})
export class FormRutinaUserPage {
  //PROPIEDADES
  public parameter:any;
  public title:any;
  public disabledBtn:boolean;
  public basePath:string = path.path;
  public data = {
    workout: '',
    client: '',
    periodo: '',
    id: '',
    state: '1',
    type: '1',
    app: localStorage.getItem('currentId')
  }
  clients:any[] = [];
  workouts:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    public toast: ToastController,
    public iab: InAppBrowser,
    public mainService: RutinasService,
    public secondService: WorkoutsService,
    public thirdService: UsersService,
    public view: ViewController
  ) {
    this.parameter = this.navParams.get('parameter');
    console.log(this.parameter)
    this.getAll(path.id);
    this.getAllSecond(path.id);
    if(this.parameter) {
      if(this.parameter.parent && this.parameter.id) {
        this.title = "Edición Rutina Día";
        //this.getSingle(this.parameter.id);  
      } else {
        this.title = "Agregar Rutina Día";
        this.data.periodo = this.parameter;
      }      
    }
  }

    //OBTENER DATOS
  getAll(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.thirdService.getClients(parameter)
    .then(response => {
      this.clients = []
      this.clients = response;
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

    //OBTENER DATOS
  getAllSecond(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.secondService.getAllUser(parameter)
    .then(response => {
      this.workouts = []
      this.workouts = response;
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss('Close');
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    console.log(this.data)
    if(this.parameter) {
      if(this.parameter.parent && this.parameter.id) {
        this.title = "Edición Rutina Día";
        //this.getSingle(this.parameter.id);  
      } else {
        this.create(this.data);
      }      
    }
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .then(response => {
      this.confirmation('Workout Agregado', 'El workout fue agregado exitosamente.');
      this.parameter = response.id;
      this.data.id = response.id;
      this.disabledBtn = false;
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .then(response => {
      this.confirmation('Workout Actualizado', 'El workout fue actualizado exitosamente.');
      this.navCtrl.pop();
      console.log(response);
      this.disabledBtn = false;
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar el workout?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando..."
            });
            load.present();
            this.mainService.delete(id)
            .then(response => {
              load.dismiss();
              this.navCtrl.pop();
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

}
