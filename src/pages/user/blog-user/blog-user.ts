import { Component } from '@angular/core';
import { NavController, LoadingController, IonicPage} from 'ionic-angular';
import { path } from '../../../app/config.module';
import { BlogService } from '../../../app/service/blog.service';

@IonicPage()
@Component({
  selector: 'blog-user',
  templateUrl: 'blog-user.html',
})
export class BlogUserPage {
  //PROPIEDADES
  public table:any[] = [];
  public months:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public mainService: BlogService
  ) {
  }

  openForm(parameter?:any) {
    this.navCtrl.push('FormBlogUserPage', { parameter });
  }

  //CARGAR LOS RETOS
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllUser(path.id)
    .then(response => {
      this.table = []
      for(let x of response) {
        let data = {
          picture: x.picture,
          title: x.title,
          description: x.description,
          created_at: this.returnDate(x.created_at),
          id: x.id
        }
        this.table.push(data);
      }
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR MESES
  public getMonths() {
    this.months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
      "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
    ];
  }

  //CAMBIAR EL FORMATO DE LA FECHA Y HORA//CAMBIAR FORMATO DE FECHA
  public returnDate(fechaJSON:any) {
    var date = new Date(fechaJSON);
    var created_at = date.getDate() + ' ' + this.months[(date.getMonth() + 1)] + ', ' + date.getFullYear();
    return created_at;
  }

  ionViewWillEnter() {
    this.getMonths();
    this.getAll();
  }

}
