import { Component } from '@angular/core';
import { NavController, IonicPage} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { path } from '../../../../app/config.module';
import { BlogService } from '../../../../app/service/blog.service';
import { LoadingController } from '../../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Clipboard } from '../../../../../node_modules/@ionic-native/clipboard';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'form-blog-user',
  templateUrl: 'form-blog-user.html',
})
export class FormBlogUserPage {
  //PROPIEDADES
  public parameter:any;
  public title:any;
  public disabledBtn:any;
  public basePath:string = path.path;
  public baseUserId = path.id;
  selectItem:any = 'actualizar';
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  public data = {
    title: '',
    description: '',
    video: '',
    picture: localStorage.getItem('currentPicture'),
    user: path.id,
    id: ''
  }

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toast: ToastController,
    public iab: InAppBrowser,
    public loading: LoadingController,
    public mainService: BlogService,
    public clipboard: Clipboard
  ) {
    this.parameter = this.navParams.get('parameter')
    if(this.parameter) {
      this.title = "Edición Blog";
      this.getSingle(this.parameter);     
    } else {
      this.title = "Agregar Blog";
    }
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    this.data.picture = $('img[alt="Avatar"]').attr('src');
    if(this.data.title) {
      if(this.data.description) {
        if(this.parameter) {
          console.log(this.data)
          this.update(this.data)
        } else {
          console.log(this.data)
          this.create(this.data)
        }
      } else {
        this.message('La descripción es requerida.');
      }
    } else {
      this.message('El título es requerido.');
    }
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .then(response => {
      this.confirmation('Blog Agregado', 'El blog fue agregado exitosamente.');
      this.data.id = response.id;
      this.parameter = response.id;
      this.title = "Edición Blog";
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .then(response => {
      this.confirmation('Blog Actualizado', 'El blog fue actualizado exitosamente.');
      this.navCtrl.pop();
      console.log(response);
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar el blog?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando..."
            });
            load.present();
            this.mainService.delete(id)
            .then(response => {
              load.dismiss();
              this.navCtrl.pop();
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //OBTENER DATOS
  getComments(parameter:any) {
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
    }).catch(error => {
      console.log(error)
    });
  }

  //IMAGEN DE CATEGORIA
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    console.log(archivos[0])
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'blog'
          },
          function(respuesta) {
            $('#imgAvatar').attr("src", respuesta.url)
            $("#"+id).val('')
          }
        );
      } else {
        this.message('La imagen es demasiado grande.')
      }
    } else {
      this.message('El tipo de imagen no es válido.')
    }
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

  //CARGAR PAGINA
  openBrowser(urlObject:any) {
    this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet,toolbarcolor=#E67E22');
  }

  //COPIAR AL PORTAPAPELES
  copyClipboard(parameter:any) {
    this.clipboard.copy(parameter);
    this.message('Zoom ID copiado al portapapeles.')
  }
}
