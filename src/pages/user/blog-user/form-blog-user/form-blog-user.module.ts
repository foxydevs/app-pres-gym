import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormBlogUserPage } from './form-blog-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormBlogUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormBlogUserPage),
    PipeModule
  ],
  exports: [
    FormBlogUserPage
  ]
})
export class FormBlogUserPageModule {}