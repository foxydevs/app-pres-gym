import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormCommerceUserPage } from './form-commerce-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormCommerceUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormCommerceUserPage),
    PipeModule
  ],
  exports: [
    FormCommerceUserPage
  ]
})
export class FormCommerceUserPageModule {}