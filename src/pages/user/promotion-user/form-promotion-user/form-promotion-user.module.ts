import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormPromotionUserPage } from './form-promotion-user';
import { PipeModule } from '../../../../pipes/pipes.module';
import { DescuentoService } from '../../../../app/service/descuentos.service';

@NgModule({
  declarations: [
    FormPromotionUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormPromotionUserPage),
    PipeModule
  ],
  exports: [
    FormPromotionUserPage
  ], providers: [
    DescuentoService
  ]
})
export class FormPromotionUserPageModule {}