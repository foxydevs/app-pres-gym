import { Component } from '@angular/core';
import { NavController, IonicPage} from 'ionic-angular';
import { LoadingController } from '../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { PromotionService } from '../../../app/service/promotion.service';
import { path } from '../../../app/config.module';
import { DescuentoService } from '../../../app/service/descuentos.service';

@IonicPage()
@Component({
  selector: 'promotion-user',
  templateUrl: 'promotion-user.html',
})
export class PromotionUserPage {
  //PROPIEDADES
  table:any[] = [];
  idUser:any = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  selectItem:any = 'promociones';

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public mainService: PromotionService,
    public secondService: DescuentoService,
  ) {}

  //PROMOTION TYPE = 1
  //DESCUENTO TYPE = 2
  openForm(type1:any, id1?:any) {
    let parameter = {
      id: id1,
      type: type1
    }
    console.log(parameter)
    this.navCtrl.push('FormPromotionUserPage', { parameter });
  }

  //CARGAR LOS RETOS
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllUser(this.idUser)
    .then(response => {
      this.table = []
      this.table = response;
      load.dismiss();
    }).catch(error => {
      load.dismiss();
      console.clear
    })
  }

  //CARGAR LOS RETOS
  public getAll2() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.secondService.getAllUser(this.idUser)
    .then(response => {
      this.table = []
      this.table = response;
      load.dismiss();
    }).catch(error => {
      load.dismiss();
      console.clear
    })
  }

  ionViewWillEnter() {
    if(this.selectItem=='promociones') {
      this.getAll();
    } else {
      this.getAll2();
    }
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll();
      refresher.complete();
    }, 2000);
  }

  //REFRESCAR
  doRefresh2(refresher) {
    setTimeout(() => {
      this.getAll2();
      refresher.complete();
    }, 2000);
  }

}
