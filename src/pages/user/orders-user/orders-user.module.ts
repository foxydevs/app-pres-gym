import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrdersUserPage } from './orders-user';
 
@NgModule({
  declarations: [
    OrdersUserPage,
  ],
  imports: [
    IonicPageModule.forChild(OrdersUserPage),
  ],
  exports: [
    OrdersUserPage
  ]
})
export class OrdersUserPageModule {}