import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage } from 'ionic-angular';
import { OrdersService } from '../../../../app/service/orders.service';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import * as moment from 'moment';
import * as momentTimezone from 'moment-timezone';

@IonicPage()
@Component({
  selector: 'detail-orders-user',
  templateUrl: 'detail-orders-user.html'
})
export class DetailOrdersUserPage{
  //PROPIEDADES
  private parameter:any;
  private positions :any
  private order = {
    name: '',
    description: '',
    unit_price: '',
    unit_prices: '',
    quantity: '',
    comment: '',
    aprobacion: '',
    picture: '',
    image: '',
    user: '',
    userProvider: '',
    fecha: '',
    fechaapro: '',
    deposito: '',
    created: '',
    create: '',
    state: '2',
    id: 0
  }
  private btnDisabled:boolean = false;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  moneda = localStorage.getItem('currentCurrency');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public loading: LoadingController,
    public orderService: OrdersService,
    public view: ViewController,
    public alertCtrl: AlertController,
  ) {
    this.parameter = this.navParams.get('parameter');3
    if(this.parameter) {
      this.getSingle(this.parameter);
    } else {
      this.closeModal();
    }
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss('Close');
  }

  public getSingle(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.orderService.getSingle(id)
    .then(res => {
        this.order.id = res.id;
        this.order.name = res.products.name;
        this.order.image = res.products.picture;
        this.order.description = res.products.description;
        this.order.quantity = res.quantity;
        this.order.unit_price = res.unit_price;
        this.order.unit_prices = this.moneda + ' ' + res.unit_price;
        this.order.comment = res.comment;
        this.order.picture = res.picture;        
        this.order.aprobacion = res.aprobacion;
        this.order.fecha = res.fechaapro;
        this.order.user = res.user;
        this.order.userProvider = res.clients.firstname + ' ' + res.clients.lastname;
        this.order.deposito = res.deposito;
        var june = momentTimezone(res.created).tz("America/Guatemala");
        this.order.create = moment(june._d).format('LLL');
        this.order.created = res.created;
        this.positions = '('+res.latitude+', '+res.longitude+')';
        load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //ACEPTAR ORDEN
  public saveChanges(id:string){
    let confirm = this.alertCtrl.create({
      title: 'Desea envíar el pedido?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.btnDisabled = true;
            this.orderService.update(this.order)
            .then(response => {
              this.view.dismiss('Aceptado');
              console.clear
            }).catch(error => {
              this.btnDisabled = false;
              console.clear
            });
          }
        }
      ]
    });
    confirm.present();
  }

  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }


}
