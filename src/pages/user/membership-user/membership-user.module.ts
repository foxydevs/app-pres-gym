import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MembershipUserPage } from './membership-user';
 
@NgModule({
  declarations: [
    MembershipUserPage,
  ],
  imports: [
    IonicPageModule.forChild(MembershipUserPage),
  ],
  exports: [
    MembershipUserPage
  ]
})
export class MembershipUserPageModule {}