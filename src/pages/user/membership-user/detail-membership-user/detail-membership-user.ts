import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage } from 'ionic-angular';
import { OrdersService } from '../../../../app/service/orders.service';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import * as moment from 'moment';
import * as momentTimezone from 'moment-timezone';

@IonicPage()
@Component({
  selector: 'detail-membership-user',
  templateUrl: 'detail-membership-user.html'
})
export class DetailMembershipUserPage{
  //PROPIEDADES
  private parameter:any;
  private positions :any
  private order = {
    name: '',
    description: '',
    unit_price: '',
    unit_prices: '',
    quantity: '',
    comment: '',
    aprobacion: '',
    picture: '',
    image: '',
    user: '',
    userProvider: '',
    fecha: '',
    fechaapro: '',
    deposito: '',
    state: '2',
    created: '',
    create: '',
    id: 0
  }
  private btnDisabled:boolean = false;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  moneda = localStorage.getItem('currentCurrency');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public loading: LoadingController,
    public orderService: OrdersService,
    public view: ViewController,
    public alertCtrl: AlertController,
  ) {
    this.parameter = this.navParams.get('parameter');3
    if(this.parameter) {
      this.getSingle(this.parameter);
    } else {
      this.closeModal();
    }
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss('Close');
  }

  public getSingle(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.orderService.getSingle(id)
    .then(response => {
        this.order.id = response.id;
        this.order.name = response.products.name;
        this.order.image = response.products.picture;
        this.order.description = response.products.description;
        this.order.quantity = response.quantity;
        this.order.unit_prices = this.moneda + ' ' + response.unit_price;
        this.order.unit_price = response.unit_price;
        this.order.comment = response.comment;
        this.order.picture = response.picture;        
        this.order.user = response.user;        
        this.order.aprobacion = response.aprobacion;
        this.order.fecha = response.fechaapro;
        this.order.userProvider = response.clients.firstname + ' ' + response.clients.lastname;
        this.order.deposito = response.deposito;
        this.order.created = response.created;
        var june = momentTimezone(response.created).tz("America/Guatemala");
        this.order.create = moment(june._d).format('LLL');
        this.positions = '('+response.latitude+', '+response.longitude+')';
        load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //ACEPTAR ORDEN
  public saveChanges(){
    let confirm = this.alertCtrl.create({
      title: 'Desea envíar el pedido?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.btnDisabled = true;
            this.orderService.update(this.order)
            .then(response => {
              this.view.dismiss('Aceptado');
              console.clear
            }).catch(error => {
              this.btnDisabled = false;
              console.clear
            });
          }
        }
      ]
    });
    confirm.present();
  }

  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }


}
