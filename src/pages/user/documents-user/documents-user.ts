import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, Platform, ToastController, IonicPage } from 'ionic-angular';
import { InAppBrowser } from '../../../../node_modules/@ionic-native/in-app-browser';
import { DocumentsService } from '../../../app/service/documents.service';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'documents-user',
  templateUrl: 'documents-user.html'
})
export class DocumentsUserPage {
  //PROPIEDADES
  private products:any[] = [];
  private parameter:any;
  private idUser:any;
  private search:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public productsService: DocumentsService,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public navParams: NavParams,
    public iab: InAppBrowser,
    public platform: Platform,
    public toast: ToastController
  ) {
    this.parameter = this.navParams.get('parameter');
    this.idUser = localStorage.getItem("currentId");
  }

  //CARGAR PRODUCTOS POR CATEGORIA
  public loadAllForCategories(id:any){
    this.productsService.getAllUser(this.idUser)
    .then(response => {
      this.products = [];
      for(let x of response) {
        if(x.category == id) {
          this.products.push(x);
        }
      }
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR PRODUCTOS
  public loadAll() {
    this.productsService.getAllUser(this.idUser)
    .then(response => {
      this.products = [];
      this.products = response;
    }).catch(error => {
      console.clear
    })
  }

  public seeOrders(parameter:any) {
    this.loading.create({
      content: "Cargando",
      duration: 750
    }).present();
    this.navCtrl.push('OrdersUserPage', { parameter });
  }

  openForm(parameter?:any) {
    this.navCtrl.push('FormDocumentUserPage', { parameter });
  }

  ionViewWillEnter() {
    if(this.parameter) {
      this.loadAllForCategories(this.parameter);
    } else {
      this.loadAll();
    }
  }

  //CARGAR PAGINA
  openBrowser(urlObject:any, title:any) {
    if (this.platform.is('android')) {
      this.iab.create('http://docs.google.com/gview?url=' + urlObject, '_blank', 'presentationstyle=pagesheet,toolbarcolor=#003D6E');      
    } else {
      this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet');
    }
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar el documento?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando...",
              duration: 2000
            })
            load.present()
            this.productsService.delete(id)
            .then(res => {
              load.dismiss();
              this.loadAll();
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //BUSCAR
  public searchTable() {
    var value = this.search.toLowerCase();
    $("#myList ion-item-sliding").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

  
  //DOCUMENTO NO DISPONIBLE
  documentEmpty() {
    this.message('El documento no se encuentra disponible.');
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }


  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      if(this.parameter) {
        this.loadAllForCategories(this.parameter);
      } else {
        this.loadAll();
      }
      refresher.complete();
    }, 2000);
  }

}
