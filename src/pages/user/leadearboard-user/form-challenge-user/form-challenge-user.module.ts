import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormChallengeUserPage } from './form-challenge-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormChallengeUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormChallengeUserPage),
    PipeModule
  ],
  exports: [
    FormChallengeUserPage
  ]
})
export class FormChallengeUserPageModule {}