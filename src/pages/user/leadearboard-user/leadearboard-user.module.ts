import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeadearboardUserPage } from './leadearboard-user';
 
@NgModule({
  declarations: [
    LeadearboardUserPage,
  ],
  imports: [
    IonicPageModule.forChild(LeadearboardUserPage),
  ],
  exports: [
    LeadearboardUserPage
  ]
})
export class LeadearboardUserPageModule {}