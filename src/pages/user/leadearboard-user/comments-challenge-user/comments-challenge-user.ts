import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { ChallengeService } from '../../../../app/service/challenge.service';
import { path } from '../../../../app/config.module';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'comments-challenge-user',
  templateUrl: 'comments-challenge-user.html'
})
export class CommentsChallengeUserPage {
  private data = {
    comment: '',
    reto : '',
    user: '',
    parent: '',
    puntaje: 0,
    picture: localStorage.getItem('currentPicture')
  }
  private comment = {
    puntaje: 0,
    id: 0
  }
  private parameter:any;
  private btnDisabled:boolean;
  private basePath:string = path.path;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public mainService: ChallengeService,
    public viewCtrl: ViewController,
    public loading: LoadingController
  ) {
    this.btnDisabled = false;
    this.parameter = this.navParams.get('parameter');
    console.log(this.parameter)
    if(this.parameter.id) {
      console.log('ACAAAAA')
      this.data.reto = this.parameter.id;
      this.data.parent = this.parameter.comment;
      this.getSingle(this.parameter.comment);
    } else if(this.parameter) {
      this.data.reto = this.parameter;
    }
    this.data.user = localStorage.getItem('currentId');
  }

  //GET SINGLE
  getSingle(id:any) {
    this.mainService.getSingleComments(id)
    .then(response => {
      this.comment.id = response.id;
      this.comment.puntaje = response.puntaje;
    }).catch(error => {
      console.clear
    });
  }

  //AGREGAR
  public add() {
    this.comment.puntaje = this.comment.puntaje + 1;
  }

  //REMOVER
  public remove() {
    this.comment.puntaje = this.comment.puntaje - 1;
  }

  //GUARDAR CAMBIOS
  public saveChanges() {
    console.log(this.data)
    this.data.picture = $('img[alt="Avatar"]').attr('src');
    if(this.data.picture == 'https://bpresentacion.s3.us-west-2.amazonaws.com/avatar/8Bjpl7wuGY88gjCBWR3nsBELyEgqEbXwPkyvWiCz.png') {
      this.data.picture = '';
      if(this.data.comment) {
        this.create(this.data);
      } else {
        this.message('Ingrese un comentario.')
      }
    } else {
      if(this.data.comment) {
        this.create(this.data)
      } else {
        this.message('Ingrese un comentario.')
      }
    } 
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.createComment(formValue)
    .then(response => {
      this.updated(this.comment);
      this.navCtrl.pop();
      this.btnDisabled = false;
    }).catch(error => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //ACTUALIZAR
  updated(formValue:any) {
    this.mainService.updateComments(formValue)
    .then(response => {
    }).catch(error => {
      console.clear
    });
  }

  //SUBIR IMAGEN
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    console.log(archivos[0])
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'comments'
          },
          function(respuesta) {
            $('#imgAvatar').attr("src", respuesta.url)
            $("#"+id).val('')
          }
        );
      } else {
        this.message('La imagen es demasiado grande.')
      }
    } else {
      this.message('El tipo de imagen no es válido.')
    }
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }
}
