import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormMotivationsUserPage } from './form-motivations-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormMotivationsUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormMotivationsUserPage),
    PipeModule
  ],
  exports: [
    FormMotivationsUserPage
  ]
})
export class FormMotivationsUserPageModule {}