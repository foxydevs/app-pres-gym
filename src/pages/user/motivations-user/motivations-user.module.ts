import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MotivationsUserPage } from './motivations-user';
 
@NgModule({
  declarations: [
    MotivationsUserPage,
  ],
  imports: [
    IonicPageModule.forChild(MotivationsUserPage),
  ],
  exports: [
    MotivationsUserPage
  ]
})
export class MotivationsUserPageModule {}