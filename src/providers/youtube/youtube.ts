/**
 * @author    ThemesBuckets <themebucketbd@gmail.com>
 * @copyright Copyright (c) 2018
 * @license   Fulcrumy
 * 
 * This file represents a provider of youtube API's.
 * File path - '../../../src/providers/youtube/youtube'
 */

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { YoutubeConfig } from '../../config/youtube';

@Injectable()
export class YoutubeProvider {

  constructor(public http: HttpClient) { }

  /**
   * ---------------------------------------------------------
   * Get Channel Details
   * ---------------------------------------------------------
   */
  getChannelDetails() {
    return Observable.create(observer => {
      this.http.get(`https://www.googleapis.com/youtube/v3/channels?key=${YoutubeConfig.googleAPI}&id=${localStorage.getItem('currentIdYoutube')}&part=snippet,contentDetails,statistics,brandingSettings`)
        .subscribe(
        res => {
          observer.next(res);
        },
        err => {
          console.log("Error occured");
          observer.error(err);
        });
    });
  }

  /**
   * ---------------------------------------------------------
   * Get Home Page Videos
   * ---------------------------------------------------------
   * @param orderBy           Order by value
   * @param nextPageToken     Next page token Id
   */
  getHomeVideos(orderBy, nextPageToken) {
    let url = '';

    return Observable.create(observer => {

      if (nextPageToken) {
        url = `https://www.googleapis.com/youtube/v3/search?key=${YoutubeConfig.googleAPI}&channelId=${localStorage.getItem('currentIdYoutube')}&part=snippet,id&order=${orderBy}&pageToken=${nextPageToken}&type=video`;
      } else {
        url = `https://www.googleapis.com/youtube/v3/search?key=${YoutubeConfig.googleAPI}&channelId=${localStorage.getItem('currentIdYoutube')}&part=snippet,id&order=${orderBy}&type=video`;
      }

      this.http.get(url).subscribe(res => {
        observer.next(res);
      }, err => {
        console.log("Error occured");
        observer.error(err);
      });
    });
  }

  /**
   * ---------------------------------------------------------
   * Get List of Videos
   * ---------------------------------------------------------
   * @param nextPageToken     Next page token Id
   */
  getVideos(nextPageToken?) {
    let url = '';
    return Observable.create(observer => {

      if (nextPageToken) {
        url = `https://www.googleapis.com/youtube/v3/search?key=${YoutubeConfig.googleAPI}&channelId=${localStorage.getItem('currentIdYoutube')}&part=snippet,id&order=date&maxResults=${YoutubeConfig.maxResults}&pageToken=${nextPageToken}&type=video`;
      } else {
        url = `https://www.googleapis.com/youtube/v3/search?key=${YoutubeConfig.googleAPI}&channelId=${localStorage.getItem('currentIdYoutube')}&part=snippet,id&order=date&maxResults=${YoutubeConfig.maxResults}&type=video`;
      }

      this.http.get(url).subscribe(res => {
        observer.next(res);
      }, err => {
        console.log("Error occured");
        observer.error(err);
      });
    });
  }

  /**
   * ---------------------------------------------------------
   * Get Playlists
   * ---------------------------------------------------------
   * @param nextPageToken     Next page token Id
   */
  getPlaylist(nextPageToken?) {
    let url = '';

    if (nextPageToken) {
      url = `https://www.googleapis.com/youtube/v3/playlists?key=${YoutubeConfig.googleAPI}&channelId=${localStorage.getItem('currentIdYoutube')}&maxResults=10&part=snippet,contentDetails&pageToken=${nextPageToken}`;
    } else {
      url = `https://www.googleapis.com/youtube/v3/playlists?key=${YoutubeConfig.googleAPI}&channelId=${localStorage.getItem('currentIdYoutube')}&maxResults=10&part=snippet,contentDetails`;
    }

    return Observable.create(observer => {
      this.http.get(url).subscribe(res => {
        observer.next(res);
      }, err => {
        console.log("Error occured");
        observer.error(err);
      });
    });
  }

  /**
   * ---------------------------------------------------------
   * Get Playlist Video Items
   * ---------------------------------------------------------
   * @param playlistId        Playlist Id
   * @param nextPageToken     Next page token Id
   */
  getPlaylistItems(playlistId, nextPageToken?) {
    return Observable.create(observer => {

      let url = '';

      if (nextPageToken) {
        url = `https://www.googleapis.com/youtube/v3/playlistItems?key=${YoutubeConfig.googleAPI}&channelId=${localStorage.getItem('currentIdYoutube')}&playlistId=${playlistId}&maxResults=${YoutubeConfig.maxResults}&part=snippet,contentDetails&pageToken=${nextPageToken}`;
      } else {
        url = `https://www.googleapis.com/youtube/v3/playlistItems?key=${YoutubeConfig.googleAPI}&channelId=${localStorage.getItem('currentIdYoutube')}&playlistId=${playlistId}&maxResults=${YoutubeConfig.maxResults}&part=snippet,contentDetails`;
      }

      this.http.get(url).subscribe(res => {
        observer.next(res);
      }, err => {
        console.log("Error occured");
        observer.error(err);
      });
    });
  }

  /**
   * ---------------------------------------------------------
   * Get Video Details
   * ---------------------------------------------------------
   * @param id      Video ID
   */
  getVideoDetails(id) {
    return Observable.create(observer => {
      this.http.get(`https://www.googleapis.com/youtube/v3/videos?key=${YoutubeConfig.googleAPI}&id=${id}&part=snippet,contentDetails,statistics`)
        .subscribe(res => {
          observer.next(res);
        }, err => {
          console.log("Error occured");
          observer.error(err);
        });
    });
  }

  /**
   * ---------------------------------------------------------
   * Search Videos
   * ---------------------------------------------------------
   * @param query       Search Query
   */
  searchVideos(query) {
    return Observable.create(observer => {
      this.http.get(`https://www.googleapis.com/youtube/v3/search?key=${YoutubeConfig.googleAPI}&channelId=${localStorage.getItem('currentIdYoutube')}&q=${query}&maxResults=${YoutubeConfig.maxResults}&part=snippet`)
        .subscribe(res => {
          observer.next(res);
        }, err => {
          console.log("Error occured");
          observer.error(err);
        });
    });
  }

  /**
   * ---------------------------------------------------------
   * Get Featured Channels
   * ---------------------------------------------------------
   * This function retrived all the featured channel of your channel.
   */
  getFeaturedChannels() {
    let feturedChannels = [];
    let count = 0;
    return Observable.create(observer => {
      this.http.get(`https://www.googleapis.com/youtube/v3/channels?key=${YoutubeConfig.googleAPI}&id=${localStorage.getItem('currentIdYoutube')}&part=snippet,contentDetails,statistics,brandingSettings`).subscribe((res: any) => {
        const channelsUrls: any = res.items[0].brandingSettings.channel.featuredChannelsUrls;
        if (channelsUrls) {
          channelsUrls.forEach(element => {
            this.http.get(`https://www.googleapis.com/youtube/v3/channels?key=${YoutubeConfig.googleAPI}&id=${element}&part=snippet,contentDetails,statistics,brandingSettings`).subscribe((result: any) => {
              count++;
              feturedChannels.push(result.items[0]);
              if (count === channelsUrls.length) {
                observer.next(feturedChannels);
              }
            });
          });
        } else {
          observer.next([]);
        }
      },
        err => {
          console.log("Error occured");
          observer.error(err);
        });
    });
  }
}
