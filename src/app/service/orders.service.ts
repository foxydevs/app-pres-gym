import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from "@angular/http";

import { path } from "../config.module";

import "rxjs/add/operator/toPromise";

@Injectable()
export class OrdersService {
  headers = new Headers({'Access-Control-Allow-Origin':'*',
  'cache-control':'no-cache',
  'server':'Apache/2.4.18 (Ubuntu)',
  'x-ratelimit-limit':'60',
  'x-ratelimit-remaining':'59'});
  options = new RequestOptions({ headers: this.headers });
  private basePath:string = path.path;

  constructor(private http:Http) {
  }

  private handleError(error:any):Promise<any> {
    console.error("ha ocurrido un error")
    console.log(error)
    return Promise.reject(error.message || error)
  }

  //Obtener Pedidos Por Usuario
  public getAll():Promise<any> {
    let url = `${this.basePath}orders`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
    })
    .catch(this.handleError)
  }

  //Obtener Un Pedido
  public getOrdersProducts(id:number):Promise<any> {
    let url = `${this.basePath}products/${id}/orders`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Crear Pedido
  public create(form):Promise<any> {
    let url = `${this.basePath}orders`
    return this.http.post(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  public pagar(form):Promise<any> {
    let url = `${this.basePath}orders/1/pagar`
    return this.http.post(url,form,this.options)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Eliminar Pedido
  public delete(id):Promise<any> {
    let url = `${this.basePath}orders/${id}`
    return this.http.delete(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Actualizar Pedido
  public update(form):Promise<any> {
    let url = `${this.basePath}orders/${form.id}`
    return this.http.put(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Obtener Un Pedido
  public getSingle(id:number):Promise<any> {
    let url = `${this.basePath}orders/${id}`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Obtener Un Pedido
  public getOrdersByUserState(id:number, state:number):Promise<any> {
    let url = `${this.basePath}users/${id}/orders/${state}`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Obtener Un Pedido
  public getForUser(id:number):Promise<any> {
    let url = `${this.basePath}users/${id}/orders`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Obtener Pedidos
  public getForClients(id:number):Promise<any> {
    let url = `${this.basePath}clients/${id}/orders`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  public findSingle(ern:number):Promise<any> {
    let url = `${this.basePath}orders/find/${ern}`
      return this.http.get(url)
      .toPromise()
      .then(response => {
        return response.json()
      })
      .catch(this.handleError)
  }

  public transaction(id:any,token:any):Promise<any> {
    let url = `${this.basePath}orders/${id}/comprobante?token=${token}`
      return this.http.get(url)
      .toPromise()
      .then(response => {
        return response.json()
      })
      .catch(this.handleError)
  }

  public pagarQPP(form):Promise<any> {
    let url = `${this.basePath}orders/1/pagarqpp`
    return this.http.post(url,form, this.options)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }


}
