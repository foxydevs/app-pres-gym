import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";

import { path } from "../config.module";

import "rxjs/add/operator/toPromise";

@Injectable()
export class NewsService {
  headers = new Headers({'Access-Control-Allow-Origin':'*',
  'cache-control':'no-cache',
  'server':'Apache/2.4.18 (Ubuntu)',
  'x-ratelimit-limit':'60',
  'x-ratelimit-remaining':'59'});
  private basePath:string = path.path;

  constructor(private http:Http) {
  }

  private handleError(error:any):Promise<any> {
    console.error("ha ocurrido un error")
    console.log(error)
    return Promise.reject(error.message || error)
  }

  //OBTENER TODAS LAS NOTICIAS
  public getAll():Promise<any> {
    let url = `${this.basePath}news`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //OBTENER NOTICIAS POR USUARIO
  public getAllUser(id:any):Promise<any> {
    let url = `${this.basePath}users/${id}/news`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
    })
    .catch(this.handleError)
  }

  //AGREGAR NOTICIA 
  public create(form):Promise<any> {
    let url = `${this.basePath}news`
    return this.http.post(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //ELIMINAR NOTICIA
  public delete(id):Promise<any> {
    let url = `${this.basePath}news/${id}`
    return this.http.delete(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //ACTUALIZAR NOTICIA
  public update(form):Promise<any> {
    let url = `${this.basePath}news/${form.id}`
    return this.http.put(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //OBTENER UNA NOTICIA
  public getSingle(id:number):Promise<any> {
    let url = `${this.basePath}news/${id}`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //GET COMENTARIOS
  public getAllComments():Promise<any> {
    let url = `${this.basePath}commentsnews`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //CREAR COMENTARIO
  public createComment(form):Promise<any> {
    let url = `${this.basePath}commentsnews`
    return this.http.post(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //OBTENER COMENTARIOS POR PRODUCTO
  public getAllCommentsByNew(id:any):Promise<any> {
    let url = `${this.basePath}comments/${id}/news`
    return this.http.get(url)
    .toPromise()
    .then(response => {
      return response.json()
    })
    .catch(this.handleError)
  }

}