import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";

import { path } from "../config.module";

import "rxjs/add/operator/toPromise";

@Injectable()
export class DescuentoService {
  headers = new Headers({'Access-Control-Allow-Origin':'*',
  'cache-control':'no-cache',
  'server':'Apache/2.4.18 (Ubuntu)',
  'x-ratelimit-limit':'60',
  'x-ratelimit-remaining':'59'});
  private basePath:string = path.path;

  constructor(private http:Http) {
  }

  private handleError(error:any):Promise<any> {
    console.error("ha ocurrido un error")
    console.log(error)
    return Promise.reject(error.message || error)
  }

  //Obtener Todo
  public getAll():Promise<any> {
    let url = `${this.basePath}descuentos`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Obtener Categorias Por Usuario
  public getAllUser(id:any):Promise<any> {
    let url = `${this.basePath}users/${id}/descuentos`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
    })
    .catch(this.handleError)
  }

  //Crear Categoria 
  public create(form):Promise<any> {
    let url = `${this.basePath}descuentos`
    return this.http.post(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Crear Categoria 
  public getByCiteClient(id):Promise<any> {
    let url = `${this.basePath}clients/${id}/descuentos`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

    //Crear Categoria 
  public getByCiteUser(id, form):Promise<any> {
    let url = `${this.basePath}descuentos/${id}/users?fechaInicio=${form.fechaInicio}&fechaFin=${form.fechaFin}`
    console.log(url)
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Eliminar Categoria
  public delete(id):Promise<any> {
    let url = `${this.basePath}descuentos/${id}`
    return this.http.delete(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Actualizar Categoria
  public update(form):Promise<any> {
    let url = `${this.basePath}descuentos/${form.id}`
    return this.http.put(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Obtener Un Categoria
  public getSingle(id:number):Promise<any> {
    let url = `${this.basePath}descuentos/${id}`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //FILTRAR POR PARAMETROShttp://backend.foxylabs.xyz/presentacion/public/api/filter/0/decuentos/1?filter=codigo
  public search(app:number, id:any, parameter:string):Promise<any> {
    let url = `${this.basePath}filter/${app}/decuentos/${id}?filter=${parameter}`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

}