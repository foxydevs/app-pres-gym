import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";

import { path } from "../config.module";

import "rxjs/add/operator/toPromise";

@Injectable()
export class EventsService {
	headers = new Headers({'Access-Control-Allow-Origin':'*',
  'cache-control':'no-cache',
  'server':'Apache/2.4.18 (Ubuntu)',
  'x-ratelimit-limit':'60',
  'x-ratelimit-remaining':'59'});
  private basePath:string = path.path;

  constructor(private http:Http) {
  }

  private handleError(error:any):Promise<any> {
    console.error("ha ocurrido un error")
    console.log(error)
    return Promise.reject(error.message || error)
  }

  //Obtener Todo
  public getAll():Promise<any> {
    let url = `${this.basePath}events`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  public getAllComments():Promise<any> {
    let url = `${this.basePath}commentsevents`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Obtener Eventos Por Usuario
  public getAllUser(id:any):Promise<any> {
    let url = `${this.basePath}users/${id}/events`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
    })
    .catch(this.handleError)
  }

  //Obtener Eventos Por Usuario
  public getAllType(id:any, type:any):Promise<any> {
    let url = `${this.basePath}users/${id}/events/tipo/${type}`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
    })
    .catch(this.handleError)
  }

  //Crear Evento 
  public create(form):Promise<any> {
    let url = `${this.basePath}events`
    return this.http.post(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Crear Comentario de Eventos
  public createComment(form):Promise<any> {
    let url = `${this.basePath}commentsevents`
    return this.http.post(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Eliminar Evento
  public delete(id):Promise<any> {
    let url = `${this.basePath}events/${id}`
    return this.http.delete(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Actualizar Evento
  public update(form):Promise<any> {
    let url = `${this.basePath}events/${form.id}`
    return this.http.put(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Obtener Un Evento
  public getSingle(id:number):Promise<any> {
    let url = `${this.basePath}events/${id}`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //OBTENER COMENTARIOS POR EVENTOS
  public getAllCommentsByEvent(id:any):Promise<any> {
    let url = `${this.basePath}comments/${id}/events`
    return this.http.get(url)
    .toPromise()
    .then(response => {
      return response.json()
    })
    .catch(this.handleError)
  }
}