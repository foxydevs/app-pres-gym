import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//import { OneSignal } from '../../node_modules/@ionic-native/onesignal';
//import { path } from './config.module';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any  = 'MenuPage';

  constructor(public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    //private oneSignal: OneSignal,
    //private alertCtrl: AlertController,
    public plt: Platform
  ) {
    this.initializeApp();
    if (this.plt.is('android')) {
      //this.handlerNotifications();
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    })
  }

  /*private handlerNotifications(){
    this.oneSignal.startInit(path.oneSignalAppId, path.sender_id);
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
    this.oneSignal.handleNotificationOpened()
    .subscribe(jsonData => {
      alert(jsonData.notification.payload.title + ', '+ jsonData.notification.payload.body)
      this.presentAlert(jsonData.notification.payload.title, jsonData.notification.payload.body);
      //console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
    });
    this.oneSignal.endInit();
    this.oneSignal.getIds().then((id) => {
      alert('One Signal' + id)
      this.presentAlert('One Signal', id);
      localStorage.setItem('currentOneSignalID', id.toString());
    });
  }

  presentAlert(header:any, subHeader:any) {
    const alert = this.alertCtrl.create({
      title: header,
      subTitle: subHeader,
      buttons: ['OK']
    });
    alert.present();
  }*/
}
