import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { MyApp } from './app.component';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClientModule } from '@angular/common/http';
import { Geolocation } from '@ionic-native/geolocation';
import { HttpClient } from '@angular/common/http';
import { AdMobFree } from '@ionic-native/admob-free';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { NgCalendarModule  } from 'ionic2-calendar';
import { Clipboard } from '@ionic-native/clipboard';

//SERVICES
import { AuthService } from './service/auth.service';
import { CategorysService } from './service/categorys.service';
import { ProductsService } from './service/products.service';
import { UsersService } from './service/users.service';
import { UsersTypesService } from './service/users-types.service';
import { EventsService } from './service/events.service';
import { OrdersService } from './service/orders.service';
import { MessagesService } from './service/messages.service';
import { TipsService } from './service/tips.service';
import { AccessService } from './service/access.service';
import { ModulesService } from './service/module.service';
import { NewsService } from './service/news.service';
import { UserEventsService } from './service/userevents.service';
import { AdoptionDogsService } from './service/adoption-dogs.service';
import { AdoptionCatsService } from './service/adoption-cats.service';
import { BlogService } from './service/blog.service';
import { WorkoutsService } from './service/workouts.service';
import { MotivationService } from './service/motivation.service';
import { ChallengeService } from './service/challenge.service';
import { ActivePauseService } from './service/activepause.service';
import { SocialService } from './service/social.service';
import { CommerceService } from './service/commerce.service';
import { PromotionService } from './service/promotion.service';
import { BenefitsService } from './service/benefits.service';
import { InformationService } from './service/information.service';
import { ScheduleService } from './service/schedule.service';
import { ProgressService } from './service/progress.service';
import { StaffService } from './service/staff.service';
import { CitesService } from './service/cites.service';
import { EventsTypeService } from './service/events-type.service';
import { MembershipService } from './service/membership.service';
import { PeriodosService } from './service/periodos.service';
import { RutinasService } from './service/rutinas.service';
import { DocumentsService } from './service/documents.service';
import { TipsBibliaService } from './service/tips-biblia.service';
import { SalesService } from './service/sales.service';

//OTROS IMPORTES
import { YoutubeProvider } from '../providers/youtube/youtube';
import { AdmobProvider } from '../providers/admob/admob';
import { StorageProvider } from '../providers/storage/storage';
import { UtilsProvider } from '../providers/utils/utils';

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    IonicImageViewerModule,
    NgCalendarModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    //SERVICIOS
    AuthService,
    CategorysService,
    ProductsService,
    UsersTypesService,
    UsersService,
    EventsService,
    UserEventsService,
    OrdersService,
    MessagesService,
    NewsService,
    TipsService,
    AccessService,
    ModulesService,
    MotivationService,
    ChallengeService,
    BlogService,
    WorkoutsService,
    SocialService,
    ActivePauseService,
    AdoptionDogsService,
    AdoptionCatsService,
    BenefitsService,
    CommerceService,
    PromotionService,
    StaffService,
    ProgressService,
    EventsTypeService,
    MembershipService,
    RutinasService,
    PeriodosService,
    CitesService,
    InformationService,
    ScheduleService,
    DocumentsService,
    TipsBibliaService,
    SalesService,
    Geolocation,
    InAppBrowser,
    //YOUTUBE
    AdMobFree,
    HttpClient,
    YoutubeProvider,
    AdmobProvider,
    StorageProvider,
    UtilsProvider,
    Clipboard,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}